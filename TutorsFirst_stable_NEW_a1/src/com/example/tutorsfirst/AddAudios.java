package com.example.tutorsfirst;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AddAudios extends Activity implements OnItemLongClickListener{

	// Layout Declaration
	TextView tv_add_audios, tv_done;
	ImageView iv_add_audios;
	ListView lv_audios_list;
	MyAdapterAddAudios adapter;
	Uri saved_audio_uri;
	String filepath;
	String audioNameOnly;
	static ArrayList<AddAudiosModel> Audios_arraylist;
	static int audio_upload_index;
	JSONArray array;
	Model_Tab3_Enter_Session_Vaules model_Tab3_Enter_Session_Vaules_obj;
	int response_size;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.audios_add);
		init();

		iv_add_audios.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(
						MediaStore.Audio.Media.RECORD_SOUND_ACTION);
				startActivityForResult(intent, 1);

			}
		});
		/*
		 * Audios_arraylist = (ArrayList<AddAudiosModel>) getIntent()
		 * .getSerializableExtra("pass_audios");
		 */

		model_Tab3_Enter_Session_Vaules_obj = (Model_Tab3_Enter_Session_Vaules) getIntent()
				.getSerializableExtra("pass_values_ob");

		if (Audios_arraylist == null) {
			Audios_arraylist = new ArrayList<AddAudiosModel>();
		}

		if(Audios_arraylist.isEmpty() && Tab3_Edit_Session_Info.responseStr!=null){
			try {
				array=new JSONArray(Tab3_Edit_Session_Info.responseStr);
				response_size=array.length();
				for(int i=0;i<array.length();i++){
					JSONObject object=array.getJSONObject(i);
					AddAudiosModel model=new AddAudiosModel(object.getString("audioname"), 0, 0, object.getString("audioID"), MainActivity.BASE_URL+"/SessionAudios/"+object.getString("audioname"));
					Audios_arraylist.add(model);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		Log.d("18-1", "ArrayList Size:  " + Audios_arraylist.size());

		adapter = new MyAdapterAddAudios(AddAudios.this, Audios_arraylist);
		lv_audios_list.setAdapter(adapter);
		lv_audios_list.setOnItemLongClickListener(this);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case 1:
				data.getDataString();
				saved_audio_uri = data.getData();

				Cursor cursor = getContentResolver().query(saved_audio_uri,
						new String[] { MediaStore.Audio.Media.DATA }, null,
						null, null);
				if (cursor.moveToNext()) {
					filepath = cursor.getString(cursor
							.getColumnIndex(MediaStore.Audio.Media.DATA));
					audioNameOnly = filepath.substring(
							filepath.lastIndexOf("/") + 1, filepath.length());

					Log.d("14c", "FilePath: " + filepath + "audioNameonly: "
							+ audioNameOnly);

				}
				cursor.close();

				File audio_file = new File(filepath);

				CacheStorageUtil_audio.saveToCache(AddAudios.this,
						audioNameOnly, audio_file);

				// add data to arraylist

				Audios_arraylist.add(new AddAudiosModel(audioNameOnly,
						resultCode, resultCode, audioNameOnly, filepath));

				adapter.notifyDataSetChanged();

				// ------------------------------

				break;
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		/* startActivity(new Intent(AddAudios.this,
		 Tab3_Enter_Session_Info.class) .putExtra("pass_audios",
		 Audios_arraylist));
		 
		TabHostActivity.mangae_tabs = "audios";

		Intent jump_tab3_enter_session = new Intent(AddAudios.this,
				TabHostActivity.class);

		Log.d("15d", "onEndAddAud"
				+ model_Tab3_Enter_Session_Vaules_obj.ed_starttime);

		jump_tab3_enter_session.putExtra("pass_audios", Audios_arraylist);
		jump_tab3_enter_session.putExtra("pass_values_ob",
				model_Tab3_Enter_Session_Vaules_obj);

		startActivity(jump_tab3_enter_session);
		finish();*/
		super.onBackPressed();
		if(response_size>0){
			audio_upload_index=response_size;
		}
	}

	public void init() {
		iv_add_audios = (ImageView) findViewById(R.id.iv_add_audios);
		lv_audios_list = (ListView) findViewById(R.id.lv_audios_list);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, final int position,
			long id) {
		AlertDialog.Builder alert=new AlertDialog.Builder(this);
		alert.setTitle("Message");
		alert.setMessage("Are you sure you want to delete this audio?");
		alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				if(response_size>0){
					if(position<response_size){
						new AsyncDeleteAudio(position).execute();
					}
				}else{
					Audios_arraylist.remove(position);
					adapter.notifyDataSetChanged();
				}
			}
		});
		alert.setNegativeButton("No", null);
		alert.show();
		return false;
	}
	
	class AsyncDeleteAudio extends AsyncTask<Void, Void, Void>{

		String audioID;
		int position;
		HttpClient httpClient;
		HttpPost httpPost;
		HttpResponse httpResponse;
		ArrayList<NameValuePair> parameters;
		String response;
		ProgressDialog progressDialog;
		
		public AsyncDeleteAudio(int position) {
			this.position=position;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=ProgressDialog.show(AddAudios.this, "", "Please wait...");
			audioID=Audios_arraylist.get(position).audioID;
			parameters=new ArrayList<NameValuePair>();
			httpClient=new DefaultHttpClient();
			httpPost=new HttpPost(MainActivity.BASE_URL+"/deleteAudio.php");
			parameters.add(new BasicNameValuePair("audioID", audioID));
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(parameters));
				httpResponse=httpClient.execute(httpPost);
				response=EntityUtils.toString(httpResponse.getEntity());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.cancel();
			response_size--;
			Audios_arraylist.remove(position);
			adapter.notifyDataSetChanged();
		}
	}
}
