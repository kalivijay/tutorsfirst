package com.example.tutorsfirst;

import java.io.Serializable;

import android.util.Log;

public class AddAudiosModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String audioName;
	public int PauseButton;
	public int PlayButton;
	public String audioID;
	public String filepath;

	public AddAudiosModel(String audioName, int PauseButton, int PlayButton,
			String audioID, String filepath) {

		Log.d("14c", "audioName in model: " + audioName);

		this.audioName = audioName;
		this.PauseButton = PauseButton;
		this.PlayButton = PlayButton;
		this.audioID = audioID;
		this.filepath = filepath;

	}

}
