package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Audios extends Activity {

	// Layout Declaration
	TextView tv_add_audios, tv_done;
	ListView lv_audios;
	MyAdapterAudios adapter;
	ArrayList<AudiosModel> Audios_arraylist = new ArrayList<AudiosModel>();
	public ProgressDialog pDialog;
	String audioIDs_with_commas = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.audios);
		init();

		new MyAsyncTask_for_get_Audios_data().execute();

		tv_add_audios.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent jump_onClickAudios = new Intent(Audios.this,
						OnClickAudios.class);
				startActivity(jump_onClickAudios);
			}
		});

		tv_done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class MyAsyncTask_for_get_Audios_data extends
			AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			Audios_arraylist.clear();

			// Showing progress dialog
			pDialog = new ProgressDialog(Audios.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			adapter = new MyAdapterAudios(Audios.this, 0, Audios_arraylist);
			Log.d("31a", Audios_arraylist + " : " + adapter);

			lv_audios.setAdapter(adapter);

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL + "GetAudio.php");

		try {

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

			Log.d("31a", "response : " + responseStr);

			// Getting JSON Array node
			JSONArray nameList = new JSONArray(responseStr);
			// usedForEdit = new JSONArray(responseStr);

			// looping through All Names

			for (int i = 0; i < nameList.length(); i++) {
				JSONObject n = nameList.getJSONObject(i);

				String audioID = n.getString("audioID");
				String audioName = n.getString("audioname");

				audioIDs_with_commas = audioIDs_with_commas + audioID + ',';

				Log.d("31b", audioID + audioName);

				/*
				 * tab2_arraylist.add(new ItemsTab2(R.drawable.ic_launcher,
				 * studentName, guardianName, email));
				 */

				Audios_arraylist.add(new AudiosModel(audioName,
						R.drawable.ic_launcher, R.drawable.ic_launcher,
						R.drawable.ic_launcher, audioID));

			}

			audioIDs_with_commas = audioIDs_with_commas.substring(0,
					audioIDs_with_commas.length() - 1);

			Log.d("1a", "id: " + audioIDs_with_commas);

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init() {

		tv_add_audios = (TextView) findViewById(R.id.tv_add_audios);
		tv_done = (TextView) findViewById(R.id.tv_done);
		lv_audios = (ListView) findViewById(R.id.lv_audios);

	}

}
