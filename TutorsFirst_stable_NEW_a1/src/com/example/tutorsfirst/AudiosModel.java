package com.example.tutorsfirst;

public class AudiosModel {

	public String audioName;
	public int stopButton;
	public int PauseButton;
	public int checkBoxImage;
	public String audioID;

	public AudiosModel(String audioName, int stopButton, int PauseButton,
			int checkBoxImage, String audioID) {

		this.audioName = audioName;
		this.stopButton = stopButton;
		this.PauseButton = PauseButton;
		this.checkBoxImage = checkBoxImage;
		this.audioID = audioID;

	}

}
