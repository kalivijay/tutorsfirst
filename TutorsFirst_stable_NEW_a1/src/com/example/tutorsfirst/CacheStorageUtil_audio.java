package com.example.tutorsfirst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.util.Log;

public class CacheStorageUtil_audio {
	public static void saveToCache(Context context, String fileName, File audioFile) {
		try {
			FileOutputStream output = new FileOutputStream(new File(
					context.getCacheDir(), fileName));
			//image.compress(Bitmap.CompressFormat.PNG, 100, output);
			Log.d("myjava", "audio saved");
			output.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
