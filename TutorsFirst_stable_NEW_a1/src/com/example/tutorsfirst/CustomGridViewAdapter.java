package com.example.tutorsfirst;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author manish.s
 *
 */
public class CustomGridViewAdapter extends ArrayAdapter<Item> {
	Context context;
	int layoutResourceId;
	ArrayList<Item> data = new ArrayList<Item>();
	boolean isDeleteEnabled;

	public CustomGridViewAdapter(Context context, int layoutResourceId,
			ArrayList<Item> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			//holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}
		holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
		holder.ivDelete=(ImageView)row.findViewById(R.id.ivDelete);
		holder.ivDelete.setOnClickListener(new OnDeleteClick(position));
		if(isDeleteEnabled){
			holder.ivDelete.setVisibility(View.VISIBLE);
		}else{
			holder.ivDelete.setVisibility(View.INVISIBLE);
		}
		Item item = data.get(position);
		//holder.txtTitle.setText(item.getTitle());
		if(item.getPath()!=null){
			Picasso.with(getContext()).load(item.getPath()).into(holder.imageItem);
		}else{
			holder.imageItem.setImageBitmap(item.getImage());
		}
		return row;

	}

	public void setDelete(boolean enable){
		isDeleteEnabled=enable;
		notifyDataSetChanged();
	}
	
	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;
 		ImageView ivDelete;
	}
	
	class OnDeleteClick implements View.OnClickListener{

		int position;
		
		public OnDeleteClick(int position) {
			this.position=position;
		}
		
		@Override
		public void onClick(View v) {
			if(ImagesInGridActivity.response_size>0){
				if(position<ImagesInGridActivity.response_size){
					new AsyncDeleteImages(position).execute();
				}
			}else{
				ImagesInGridActivity.gridArray.remove(position);
				notifyDataSetChanged();
			}
		}
		
	}
	
	class AsyncDeleteImages extends AsyncTask<Void, Void, Void>{

		String imageID;
		int position;
		HttpClient httpClient;
		HttpPost httpPost;
		HttpResponse httpResponse;
		ArrayList<NameValuePair> parameters;
		String response;
		ProgressDialog progressDialog;
		
		public AsyncDeleteImages(int position) {
			this.position=position;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=ProgressDialog.show(getContext(), "", "Please wait...");
			imageID=data.get(position).id;
			parameters=new ArrayList<NameValuePair>();
			httpClient=new DefaultHttpClient();
			httpPost=new HttpPost(MainActivity.BASE_URL+"/deleteSessionImages.php");
			parameters.add(new BasicNameValuePair("imageID", imageID));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(parameters));
				httpResponse=httpClient.execute(httpPost);
				response=EntityUtils.toString(httpResponse.getEntity());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.cancel();
			ImagesInGridActivity.response_size--;
			ImagesInGridActivity.gridArray.remove(position);
			notifyDataSetChanged();
		}
	}
}