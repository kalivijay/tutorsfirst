package com.example.tutorsfirst;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomViewForAddAudios extends LinearLayout implements
		OnTouchListener, OnCompletionListener, OnBufferingUpdateListener,
		View.OnClickListener {

	// Layout Declaration
	TextView tv_audio_name;
	// ImageView lv_stopButton, lv_PauseButton, lv_checkBoxImage;
	ImageView lv_PauseButton;
	public ProgressDialog pDialog;

	private MediaPlayer mediaPlayer;

	boolean check_or_not = false;

	int position;

	MyListener ob;

	public CustomViewForAddAudios(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForAddAudios(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForAddAudios(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();

		tv_audio_name = (TextView) findViewById(R.id.tv_audio_name);
		// lv_stopButton = (ImageView) findViewById(R.id.lv_stopButton);
		lv_PauseButton = (ImageView) findViewById(R.id.lv_PauseButton_add_audio);
		// lv_checkBoxImage = (ImageView) findViewById(R.id.lv_checkBoxImage);

		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);

	}

	public void setOnMyClick(MyListener ob) {
		this.ob = ob;
	}

	public void displayContent(AddAudiosModel audiosModel_ob, int position) {

		Log.d("14c", audiosModel_ob.audioName);

		tv_audio_name.setText(audiosModel_ob.audioName);
		final String b = audiosModel_ob.audioName;
		// lv_stopButton.setBackgroundResource(R.drawable.ic_launcher);
		//lv_PauseButton.setBackgroundResource(R.drawable.play_btn);
		// lv_checkBoxImage.setBackgroundResource(R.drawable.ic_launcher);

		Log.d("14c", "audio name:  " + audiosModel_ob.audioName);

		this.position = position;

		Log.d("javabasic", "postion:  " + position);
		final String a_for_filepath = audiosModel_ob.filepath;

		lv_PauseButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.d("javabasic", "on play");
				try {

					// String a = MainActivity.BASE_URL + "SessionAudios/" + b;
					
					mediaPlayer.setDataSource(a_for_filepath);

					Log.d("javabasic", a_for_filepath);

					mediaPlayer.prepare();

				} catch (Exception e) {
					e.printStackTrace();
				}

				if (!mediaPlayer.isPlaying()) {
					mediaPlayer.start();
					lv_PauseButton.setImageResource(R.drawable.pause_btn);
				} else {
					mediaPlayer.pause();
					lv_PauseButton.setImageResource(R.drawable.play_btn);
				}
			}
		});

		/*
		 * lv_stopButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View arg0) { // TODO Auto-generated
		 * method stub try { // String a = //
		 * "http://192.168.2.15/TutorsApi/SessionAudios/recording_2014_7_31_14_21_8.amr"
		 * ;
		 * 
		 * String a = MainActivity.BASE_URL + "SessionAudios/" + b;
		 * 
		 * mediaPlayer.setDataSource(a);
		 * 
		 * mediaPlayer.prepare();
		 * 
		 * } catch (Exception e) { e.printStackTrace(); } if
		 * (mediaPlayer.isPlaying()) { mediaPlayer.stop(); //
		 * buttonPlayPause.setImageResource(R.drawable.button_pause); } } });
		 * 
		 * lv_checkBoxImage.setOnClickListener(this);
		 */
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		return false;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * @Override public void onClick(View v) { Log.d("javabasic",
	 * "on click checkbox");
	 * 
	 * if (check_or_not) {
	 * lv_checkBoxImage.setBackgroundResource(R.drawable.uncheck_btn);
	 * check_or_not = false; ob.MyClick(position, false);
	 * 
	 * } else { lv_checkBoxImage.setBackgroundResource(R.drawable.check_btn);
	 * check_or_not = true; ob.MyClick(position, true);
	 * 
	 * }
	 */
	// }

}
