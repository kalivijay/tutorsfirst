package com.example.tutorsfirst;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomViewForTab2 extends RelativeLayout {

	// Layout Declaration
	TextView tvStudentName, tvGuardianName;
	ImageView iv_checkboxs_red;
	TextView  tvArrow;
	// ImageView iv_tab2_std;
	boolean check_or_not = false;
	MyListener listener;
	int position;

	public CustomViewForTab2(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForTab2(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForTab2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();

		tvStudentName = (TextView) findViewById(R.id.tvStudentName);
		tvGuardianName = (TextView) findViewById(R.id.tvGuardianName);

		// iv_tab2_std = (ImageView) findViewById(R.id.iv_tab2_std);
		iv_checkboxs_red = (ImageView) findViewById(R.id.ivCheck);
		tvArrow=(TextView)findViewById(R.id.tvArrow);
	}

	public void displayContent(ItemsTab2 itemsTab2_ob, int list_position,
			boolean chceckbox_show_or_not) {
		position = list_position;
		if (chceckbox_show_or_not == true) {
			iv_checkboxs_red.setVisibility(View.GONE);

		} else {
			tvArrow.setVisibility(View.GONE);
		}

		if (!itemsTab2_ob.isChecked) {
			iv_checkboxs_red.setImageResource(R.drawable.uncheck_btn);
		} else {
			iv_checkboxs_red.setImageResource(R.drawable.check_btn);
		}

		iv_checkboxs_red.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Log.d("234", "you clicked");

				if (check_or_not) {
					/*iv_checkboxs_red
							.setImageResource(R.drawable.uncheck_btn);*/
					check_or_not = false;
					listener.MyClick(position, false);
				} else {
					/*iv_checkboxs_red
							.setImageResource(R.drawable.check_btn);*/
					check_or_not = true;
					Log.d("234", "checed");
					listener.MyClick(position, true);
				}

			}
		});

		tvStudentName.setText(itemsTab2_ob.stdFirstName+" "+itemsTab2_ob.stdLastName);

		tvGuardianName.setText("Guardian: " + itemsTab2_ob.guarFirstName+" "+itemsTab2_ob.guarLastName);
		// iv_tab2_std.setImageResource(itemsTab2_ob.stdImage);

	}

	public void setOnCheckedListener(MyListener listener) {
		this.listener = listener;
	}
}
