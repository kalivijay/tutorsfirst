package com.example.tutorsfirst;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomViewForTab3 extends RelativeLayout {

	// Layout Declaration
	TextView tv_first_val, tv_second_val, tv_third_val, tv_fourth_val,
			tv_fifth_val;
	ImageView iv_tab3_session;

	public CustomViewForTab3(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForTab3(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CustomViewForTab3(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();

		tv_first_val = (TextView) findViewById(R.id.tvStudentName);
		tv_second_val = (TextView) findViewById(R.id.tvSessionDate);
		tv_third_val = (TextView) findViewById(R.id.tvSubject);
		tv_fourth_val = (TextView) findViewById(R.id.tvSessionTime);
		/*tv_fifth_val = (TextView) findViewById(R.id.tv_fifth_val);

		iv_tab3_session = (ImageView) findViewById(R.id.iv_tab3_session);*/

	}

	public void displayContent(ItemsTab3 itemsTab3_ob, int list_position) {

		tv_first_val.setText(itemsTab3_ob.firstVal);
		tv_second_val.setText(itemsTab3_ob.secondVal);
		tv_third_val.setText(itemsTab3_ob.thirdVal);
		tv_fourth_val.setText(itemsTab3_ob.fourthVal);
		/*tv_fifth_val.setText(itemsTab3_ob.fifthVal);*/

		//iv_tab3_session.setImageResource(itemsTab3_ob.sessionImage);

	}

}
