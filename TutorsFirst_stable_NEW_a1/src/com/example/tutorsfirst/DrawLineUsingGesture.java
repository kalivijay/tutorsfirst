package com.example.tutorsfirst;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.TextView;

public class DrawLineUsingGesture extends Activity implements OnClickListener {

	// Button DoneButton;
	static String globelTime_for_signature;
	public ProgressDialog pDialog2;
	TextView tvSignature,tvClear, tvCancel, tvDone;
	GestureOverlayView gestureView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.draw_line_using_gesture);
		init();
	}

	private void init() {
		gestureView = (GestureOverlayView) findViewById(R.id.signaturePad);
		tvSignature=(TextView)findViewById(R.id.tvSignature);
		tvClear = (TextView) findViewById(R.id.tvClear);
		tvCancel = (TextView) findViewById(R.id.tvCancel);
		tvDone = (TextView) findViewById(R.id.tvDone);
		SpannableString content = new SpannableString("Clear");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		tvClear.setText(content);
		tvSignature.setText(getIntent().getStringExtra("title"));
		tvClear.setOnClickListener(this);
		tvCancel.setOnClickListener(this);
		tvDone.setOnClickListener(this);
	}

	public void saveSig() {

		try {
			gestureView.setDrawingCacheEnabled(true);

			Bitmap signature_image = Bitmap.createBitmap(gestureView
					.getDrawingCache());
			// new code
			globelTime_for_signature = SignUp.getCurrentTimeStamp() + ".png";
			Log.d("draw", globelTime_for_signature + "");
			CacheStorageUtil.saveToCache(DrawLineUsingGesture.this,
					globelTime_for_signature, signature_image);
			// new sendImage().execute();
			// --
			File f = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "signature.png");
			f.createNewFile();
			FileOutputStream os = new FileOutputStream(f);
			os = new FileOutputStream(f);
			// compress to specified format (PNG), quality - which is ignored
			// for PNG, and out stream
			signature_image.compress(Bitmap.CompressFormat.PNG, 100, os);
			gestureView.clear(false);
			os.close();

		} catch (Exception e) {
			Log.v("Gestures", e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog2 = new ProgressDialog(DrawLineUsingGesture.this);
			pDialog2.setMessage("Please wait...");
			pDialog2.setCancelable(false);
			pDialog2.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			uploadImage();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar
			Log.d("newtime", "upif");
			if (pDialog2.isShowing()) {
				pDialog2.dismiss();
				Log.d("newtime", "inif");

			}
			Log.d("newtime", "bottomif");

		}
	}

	public void uploadImage() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();
		File file = new File(getCacheDir(), globelTime_for_signature);

		mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		mpBuilder.addPart("file", new FileBody(file));
		HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
				+ "upload_ProfileImage.php");
		HttpClient httpClient = new DefaultHttpClient();
		httpPost_image.setEntity(mpBuilder.build());

		try {
			HttpResponse httpResponse = httpClient.execute(httpPost_image);

			String response = EntityUtils.toString(httpResponse.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// return null;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.tvClear:
			gestureView.cancelClearAnimation();
			gestureView.clear(true);
			break;
		case R.id.tvCancel:
			finish();
			break;
		case R.id.tvDone:
			saveSig();
			finish();
			break;
		}
	}
}
