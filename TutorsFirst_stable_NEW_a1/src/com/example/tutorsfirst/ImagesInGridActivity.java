package com.example.tutorsfirst;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ImagesInGridActivity extends Activity{
	GridView gridView;
	static ArrayList<Item> gridArray = new ArrayList<Item>();
	static int image_upload_index;
	CustomGridViewAdapter customGridAdapter;
	ImageView ivDelete;
	JSONArray array;

	Button btn_save;
	TextView btn_open_gallary;

	Bitmap image;

	ArrayList<File> fileList;

	public ProgressDialog pDialog2;
	public ProgressDialog pDialog;

	// String imageNameOnly;
	ArrayList<String> imageNameOnly_array = new ArrayList<String>();
	
	static int response_size;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.images_in_grid_activity);
		fileList = new ArrayList<File>();
		init();
		gridView = (GridView) findViewById(R.id.gridView1);

		if(gridArray.isEmpty() && Tab3_Edit_Session_Info.responseStr!=null){
			try {
				array=new JSONArray(Tab3_Edit_Session_Info.responseStr);
				response_size=array.length();
				for(int i=0;i<array.length();i++){
					JSONObject obj=array.getJSONObject(i);
					gridArray.add(new Item(MainActivity.BASE_URL+"/SessionImages/"+obj.getString("imagename"),obj.getString("imageID")));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		Log.d("18f", "size: " + gridArray.size() + "");
		customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid,
				gridArray);
		gridView.setAdapter(customGridAdapter);
		btn_open_gallary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * startActivityForResult( new
				 * Intent(Intent.ACTION_PICK).setType("image/*"), 1);
				 */

				showSelectImageDialog();

			}
		});
		
		ivDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(customGridAdapter.isDeleteEnabled){
					customGridAdapter.setDelete(false);
				}else{
					customGridAdapter.setDelete(true);
				}
			}
		});

		/*
		 * btn_save.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub new sendImage().execute(); new GetNames().execute();
		 * 
		 * } });
		 */

	}

	public void init() {
		btn_open_gallary = (TextView) findViewById(R.id.btn_open_gallary);
		SpannableString content = new SpannableString("Add Images");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		btn_open_gallary.setText(content);
		ivDelete=(ImageView)findViewById(R.id.ivDelete);
		// btn_save = (Button) findViewById(R.id.btn_save);

	}

	private void showSelectImageDialog() {
		Log.d("one", "one");
		AlertDialog.Builder builder = new AlertDialog.Builder(
				ImagesInGridActivity.this);
		Log.d("one", "two");
		builder.setTitle("Select Image");
		builder.setItems(getResources().getStringArray(R.array.select_image),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						int pos = (int) Math.round(item);
						/*
						 * tv_select_image.setText(getResources().getStringArray(
						 * R.array.select_image)[pos]);
						 */
						// To open the gallery
						if (pos == 0) {
							Intent intent = new Intent(
									"android.media.action.IMAGE_CAPTURE");

							startActivityForResult(intent, 0);

						} else if (pos == 1) {
							startActivityForResult(new Intent(
									Intent.ACTION_PICK).setType("image/*"), 1);
						}
						Log.d("tuvk1", "pos" + pos);

						dialog.dismiss();
					}
				});

		AlertDialog alertBox = builder.create();
		alertBox.show();
	}

	// To fetch the image from gallery
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

			Cursor cursor = getContentResolver().query(data.getData(),
					new String[] { MediaStore.Images.Media.DATA }, null, null,
					null);
			if (cursor.moveToNext()) {
				String filePath = cursor.getString(cursor
						.getColumnIndex(MediaStore.Images.Media.DATA));

				/*
				 * imageNameOnly = filePath.substring( filePath.lastIndexOf("/")
				 * + 1, filePath.length());
				 */

				imageNameOnly_array.add(filePath.substring(
						filePath.lastIndexOf("/") + 1, filePath.length()));

				/*
				 * imageNameOnly_array.add(filePath
				 * .substring(filePath.lastIndexOf("/") + 1,
				 * filePath.length()).replace('[', ' ') .replace(']',
				 * ' ').trim());
				 */

				Log.d("31z", imageNameOnly_array + "");

				fileList.add(new File(filePath));

				image = BitmapFactory.decodeFile(filePath);

				// btn_images.setImageBitmap(image);

				Log.d("31c", filePath);

				gridArray.add(new Item(image));

				Log.d("31d", gridArray + "");

				customGridAdapter = new CustomGridViewAdapter(this,
						R.layout.row_grid, gridArray);
				gridView.setAdapter(customGridAdapter);

			}
			cursor.close();

		} else if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			if (data != null) {
				image = (Bitmap) data.getExtras().get("data");
				// iv_user_pic_tab1.setImageBitmap(image);
				Log.d("one", "image: " + data.getExtras().get("data"));

				gridArray.add(new Item(image));

				Log.d("31d", gridArray + "");

				customGridAdapter = new CustomGridViewAdapter(this,
						R.layout.row_grid, gridArray);
				gridView.setAdapter(customGridAdapter);

			}
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog2 = new ProgressDialog(ImagesInGridActivity.this);
			pDialog2.setMessage("Please wait...");
			pDialog2.setCancelable(false);
			pDialog2.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			uploadImage();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar
			Log.d("newtime", "upif");
			if (pDialog2.isShowing()) {
				pDialog2.dismiss();
				Log.d("newtime", "inif");

			}
			Log.d("newtime", "bottomif");

		}
	}

	public void uploadImage() {
		for (File file : fileList) {
			MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();
			mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			mpBuilder.addPart("file", new FileBody(file));
			HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
					+ "upload_SessionImages.php");
			HttpClient httpClient = new DefaultHttpClient();
			httpPost_image.setEntity(mpBuilder.build());
			try {
				HttpResponse httpResponse = httpClient.execute(httpPost_image);
				String response = EntityUtils
						.toString(httpResponse.getEntity());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// return null;
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetNames extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(ImagesInGridActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

		}
	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		for (String imageNameOnly : imageNameOnly_array) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(MainActivity.BASE_URL
					+ "upload_SessionImagesInfo.php");

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// these name,email,phone,password and userpic is SAME as define
				// in
				// php file.

				nameValuePairs.add(new BasicNameValuePair("imagename",
						imageNameOnly));
				Log.d("31last", imageNameOnly);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);

			}/*
			 * catch (ClientProtocolException e) { // TODO Auto-generated catch
			 * block }
			 */catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if(response_size>0){
			image_upload_index=response_size;
		}
	}
}
