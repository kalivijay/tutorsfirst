package com.example.tutorsfirst;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ImagesOnSession extends Activity {

	Button btn_open_gallary;

	Bitmap image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.images_on_session);

		init();

		btn_open_gallary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(
						new Intent(Intent.ACTION_PICK).setType("image/*"), 1);
			}
		});

	}

	public void init() {
		btn_open_gallary = (Button) findViewById(R.id.btn_open_gallary);

	}

	// To fetch the image from gallery
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

			Cursor cursor = getContentResolver().query(data.getData(),
					new String[] { MediaStore.Images.Media.DATA }, null, null,
					null);
			if (cursor.moveToNext()) {
				String filePath = cursor.getString(cursor
						.getColumnIndex(MediaStore.Images.Media.DATA));
				image = BitmapFactory.decodeFile(filePath);

				// btn_images.setImageBitmap(image);

			}
			cursor.close();

		}

	}

}
