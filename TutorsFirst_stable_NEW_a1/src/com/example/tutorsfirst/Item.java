package com.example.tutorsfirst;

import java.io.Serializable;

import android.graphics.Bitmap;

/**
 * 
 * @author manish.s
 * 
 */

public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id;
	Bitmap image;
	String path;

	// String title;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Item(Bitmap image) {
		super();
		this.image = image;
		// this.title = title;
	}
	
	public Item(String path,String id){
		this.path=path;
		this.id=id;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}
	/*
	 * public String getTitle() { return title; }
	 */
	/*
	 * public void setTitle(String title) { this.title = title; }
	 */

}
