package com.example.tutorsfirst;

public class ItemsTab2 {

	public int stdImage;
	public String stdFirstName;
	public String stdLastName;
	public String guarFirstName;
	public String guarLastName;
	public boolean isChecked;
	public String studentID_send_to_model;

	public ItemsTab2(int stdImage, String stdFirstName, String stdLastName,
			String guarFirstName, String guarLastName,
			String studentID_send_to_model, boolean isChecked) {

		// this.stdImage = stdImage;
		this.stdFirstName = stdFirstName;
		this.stdLastName = stdLastName;
		this.guarFirstName = guarFirstName;
		this.guarLastName = guarLastName;
		this.isChecked = isChecked;
		this.studentID_send_to_model = studentID_send_to_model;
	}

}
