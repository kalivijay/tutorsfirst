package com.example.tutorsfirst;

public class ItemsTab3 {

	public int sessionImage;
	public String firstVal;
	public String secondVal;
	public String thirdVal;
	public String fourthVal;
	public String fifthVal;
	public String sign_path;
	
	public ItemsTab3(int sessionImage, String firstVal, String secondVal,
			String thirdVal, String fourthVal, String fifthVal,String sign_path) {

		this.sessionImage = sessionImage;
		this.firstVal = firstVal;
		this.secondVal = secondVal;
		this.thirdVal = thirdVal;
		this.fourthVal = fourthVal;
		this.fifthVal = fifthVal;
		this.sign_path=sign_path;
	}
}
