package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	// Layout Declaration
	// Button btn_sign_up, btn_login;
	// SERVER BASE URL
	static String BASE_URL = "http://pairroxz.com/developer/";
	TextView btn_login, tv_forgetPass;
	ImageView btn_sign_up;
	EditText ed_username, ed_password;
	public ProgressDialog pDialog;
	public String login_correct_or_not = "no";
	Intent jump_tab1;
	String responseStr;
	String email, password, name, phone, userpic, tutorID, lastName;
	JSONArray arr;
	static String one_username, one_password;
	public String usr_or_pass_wrong = "check";
	static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_main);

		init();
		// tv_forgetPass
		Paint p = new Paint();
		p.setColor(Color.RED);

		// TextView t = (TextView) findViewById(R.id.textview);
		tv_forgetPass.setPaintFlags(p.getColor());
		tv_forgetPass.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

		btn_sign_up.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent jump_sign_up = new Intent(MainActivity.this,
						SignUp.class);
				startActivity(jump_sign_up);
			}
		});

		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (ed_username.getText().toString().matches(emailPattern)
						&& ed_username.getText().toString().length() > 0) {
					if (ed_password.getText().toString().length() == 0
							|| ed_username.getText().toString().length() == 0) {

						// showing dialog
						AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
								MainActivity.this);
						dlgAlert.setMessage("Fill All the Fields");
						dlgAlert.setTitle("App Title");
						dlgAlert.setPositiveButton("OK", null);
						dlgAlert.setCancelable(true);
						dlgAlert.create().show();

					}

					else {
						new sendNames().execute();

					}
				} else {
					// showing dialog
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							MainActivity.this);
					dlgAlert.setMessage("Invalid Email Address");
					dlgAlert.setTitle("App Title");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();

				}

			}
		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendNames extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// it is required to put these statements in onPosteExecute you know
			// why !

			// if (arr.length().re) {
			/* arr = arr.remove(0); */

			

			if (usr_or_pass_wrong.equalsIgnoreCase("wrong")) {
				AlertDialog.Builder alert=new AlertDialog.Builder(MainActivity.this);
				alert.setTitle("Invalid Login");
				alert.setMessage("Invalid Username or Password");
				alert.setPositiveButton("OK", null);
				alert.show();
			} else {
				jump_tab1 = new Intent(MainActivity.this, TabHostActivity.class);
				jump_tab1.putExtra("email", email);
				jump_tab1.putExtra("name", name);
				jump_tab1.putExtra("password", password);
				jump_tab1.putExtra("phone", phone);
				jump_tab1.putExtra("last_name", lastName);
				jump_tab1.putExtra("userpic", userpic);
				jump_tab1.putExtra("tutorID", tutorID);
				startActivity(jump_tab1);
			}

			// }

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data and get Response as JSON.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "GetProfileData.php");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			one_username = ed_username.getText().toString();
			one_password = ed_password.getText().toString();

			nameValuePairs.add(new BasicNameValuePair("email", ed_username
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("password", ed_password
					.getText().toString()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			Log.d("NewResponse", "start:  " + response);

			// now i convert the respone in responseStr

			responseStr = EntityUtils.toString(response.getEntity());

			Log.d("NewResponse", "start:  " + responseStr);

			// i above line i can see appropriate JSON.

			// now i get all json items saperately

			arr = new JSONArray(responseStr);

			Log.d("mangal", arr + "");

			if (arr.length() == 0) {
				usr_or_pass_wrong = "wrong";
			} else {
				usr_or_pass_wrong = "right";
			}

			Log.d("testing", arr.length() + "");

			if (arr.length() != 0) {
				Log.d("javavk", arr + "");

				JSONObject jObj = arr.getJSONObject(0);

				email = jObj.getString("email");
				name = jObj.getString("name");
				password = jObj.getString("password");
				phone = jObj.getString("phone");
				userpic = jObj.getString("userpic");
				lastName = jObj.getString("lastname");
				tutorID = jObj.getString("tutorID");

				Tab1Activity.tutorID = tutorID;

				Log.d("kaliandme", email + name + password + phone + userpic
						+ ":  lastname   :" + lastName);

			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void init() {
		ed_password = (EditText) findViewById(R.id.ed_password);
		ed_username = (EditText) findViewById(R.id.ed_username);
		// btn_sign_up = (Button) findViewById(R.id.btn_sign_up);
		// btn_login = (Button) findViewById(R.id.btn_login);
		btn_sign_up = (ImageView) findViewById(R.id.btn_sign_up);
		btn_login = (TextView) findViewById(R.id.btn_login);
		tv_forgetPass = (TextView) findViewById(R.id.tv_forgetPass);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

		this.finish();

	}

}
