package com.example.tutorsfirst;

import java.io.Serializable;

import android.util.Log;

public class Model_Tab3_Enter_Session_Vaules implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String ed_selectsubject;
	public String ed_session;
	public String ed_outoftotal;
	public String ed_starttime;
	public String ed_endtime;
	public String ed_nextsessiondate;
	public String ed_nextsessiontime;
	public String ed_sessionnotes;

	public Model_Tab3_Enter_Session_Vaules(String ed_selectsubject,
			String ed_session, String ed_outoftotal, String ed_starttime,
			String ed_endtime, String ed_nextsessiondate,
			String ed_nextsessiontime, String ed_sessionnotes) {
		this.ed_selectsubject = ed_selectsubject;
		this.ed_session = ed_session;
		this.ed_outoftotal = ed_outoftotal;
		this.ed_starttime = ed_starttime;
		this.ed_endtime = ed_endtime;
		this.ed_nextsessiondate = ed_nextsessiondate;
		this.ed_nextsessiontime = ed_nextsessiontime;
		this.ed_sessionnotes = ed_sessionnotes;

	}

}
