package com.example.tutorsfirst;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MyAdapter extends ArrayAdapter<ItemsTab2> implements MyListener {

	List<ItemsTab2> list;
	private LayoutInflater inflater;

	// checkBoxes a boolean variable created by me for checking i where the
	// activity called.
	boolean checkBoxes;

	public MyAdapter(Context context, int resource, List<ItemsTab2> objects,
			boolean checkBoxes) {
		super(context, resource, objects);
		this.checkBoxes = checkBoxes;
		this.list = objects;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// View row = convertView;

		CustomViewForTab2 customViewForTab2_ob = null;

		if (convertView == null) {

			customViewForTab2_ob = (CustomViewForTab2) inflater.inflate(
					R.layout.customview_tab2, null);
		}

		else {
			customViewForTab2_ob = (CustomViewForTab2) convertView;

		}

		customViewForTab2_ob.displayContent(list.get(position), position,
				checkBoxes);
		customViewForTab2_ob.setOnCheckedListener(this);

		return customViewForTab2_ob;
	}

	@Override
	public void MyClick(int position, boolean value) {
		for (ItemsTab2 obj : list) {
			obj.isChecked = false;
		}
		list.get(position).isChecked = value;

		Log.d("peh", "" + list.get(position).isChecked);
		Log.d("peh", "" + list.get(position).studentID_send_to_model);
		Log.d("pehv", "" + list.get(position).stdFirstName);
		Tab3_Enter_Session_Info.studentID_of_selected_studnet = list
				.get(position).studentID_send_to_model + "";
		Tab3_Enter_Session_Info.select_stuent_label = list.get(position).stdFirstName +" "+list.get(position).stdLastName;

		notifyDataSetChanged();
		Intent myIntent = new Intent(getContext(),
				TabHostActivity.class);
		TabHostActivity.mangae_tabs = "3";
		getContext().startActivity(myIntent);
	}
}
