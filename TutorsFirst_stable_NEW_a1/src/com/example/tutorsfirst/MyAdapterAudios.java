package com.example.tutorsfirst;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MyAdapterAudios extends ArrayAdapter<AudiosModel> implements
		MyListener {

	List<AudiosModel> list;
	private LayoutInflater inflater;
	String arr_id[];
	public static String s;

	boolean checkBoxes;

	public MyAdapterAudios(Context context, int resource,
			List<AudiosModel> objects) {
		super(context, resource, objects);

		this.list = objects;

		arr_id = new String[objects.size()];

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// View row = convertView;

		CustomViewForAudios customViewForAudios_ob = null;

		if (convertView == null) {
			customViewForAudios_ob = (CustomViewForAudios) inflater.inflate(
					R.layout.customview_for_audios, null);
		}

		else {

			customViewForAudios_ob = (CustomViewForAudios) convertView;
		}

		customViewForAudios_ob.displayContent(list.get(position), position);
		
		customViewForAudios_ob.setOnMyClick(this);
		
		return customViewForAudios_ob;
	}

	@Override
	public void MyClick(int position, boolean value) {
		// TODO Auto-generated method stub
		Log.d("1b", "hi :  " + position);
		if (value) {
			arr_id[position] = list.get(position).audioID;
		} else {
			arr_id[position] = null;
		}

		s = "";
		for (String id : arr_id) {
			if(id!=null){
				s=s+id+",";
			}
		}
		if(s.length()>0){
			s=s.substring(0, s.length()-1);
		}
		Log.i("arr_id", s);

	}
}
