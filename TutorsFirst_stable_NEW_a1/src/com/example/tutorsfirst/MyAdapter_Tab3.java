package com.example.tutorsfirst;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MyAdapter_Tab3 extends ArrayAdapter<ItemsTab3> {

	List<ItemsTab3> list_tab3;
	private LayoutInflater inflater;

	public MyAdapter_Tab3(Context context, int resource, List<ItemsTab3> objects) {
		super(context, resource, objects);

		this.list_tab3 = objects;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// View row = convertView;

		CustomViewForTab3 customViewForTab3_ob = null;

		if (convertView == null) {
			customViewForTab3_ob = (CustomViewForTab3) inflater.inflate(
					R.layout.customview_tab3, null);
		}

		else {
			customViewForTab3_ob = (CustomViewForTab3) convertView;

		}

		customViewForTab3_ob.displayContent(list_tab3.get(position), position);
		return customViewForTab3_ob;

	}
}
