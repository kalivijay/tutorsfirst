package com.example.tutorsfirst;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class OnClickAudios extends Activity {

	// Layout Declaration
	Button btn_record, btn_save;
	Uri saved_audio_uri;
	public ProgressDialog pDialog2;
	public ProgressDialog pDialog;
	String filepath, audioNameOnly;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.on_click_audios);
		init();

		btn_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final int ACTIVITY_RECORD_SOUND = 1;
				Intent intent = new Intent(
						MediaStore.Audio.Media.RECORD_SOUND_ACTION);
				startActivityForResult(intent, 1);
			}
		});

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new sendAudioName().execute();
				new sendAudio().execute();
			}
		});

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case 1:
				data.getDataString();
				saved_audio_uri = data.getData();
				Toast.makeText(OnClickAudios.this,
						"Saved: " + saved_audio_uri.getPath(),
						Toast.LENGTH_LONG).show();

				Cursor cursor = getContentResolver().query(saved_audio_uri,
						new String[] { MediaStore.Audio.Media.DATA }, null,
						null, null);
				if (cursor.moveToNext()) {
					filepath = cursor.getString(cursor
							.getColumnIndex(MediaStore.Audio.Media.DATA));
					audioNameOnly = filepath.substring(
							filepath.lastIndexOf("/") + 1, filepath.length());
					Toast.makeText(getApplicationContext(), filepath,
							Toast.LENGTH_LONG).show();
					Log.d("30b", audioNameOnly);
				}
				cursor.close();

				break;
			}
		}
	}

	public void init() {
		btn_record = (Button) findViewById(R.id.btn_record);
		btn_save = (Button) findViewById(R.id.btn_save);
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendAudio extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog2 = new ProgressDialog(OnClickAudios.this);
			pDialog2.setMessage("Please wait...");
			pDialog2.setCancelable(false);
			pDialog2.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			uploadAudio();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar
			Log.d("newtime", "upif");
			// if (pDialog2.isShowing()) {
			pDialog2.dismiss();
			// Log.d("newtime", "inif");

			// }
			// Log.d("newtime", "bottomif");

		}
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendAudioName extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(OnClickAudios.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendAudioNameToPhpFile();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar
			Log.d("newtime", "upif");
			// if (pDialog2.isShowing()) {
			pDialog.dismiss();
			Log.d("newtime", "inif");

			// }
			// Log.d("newtime", "bottomif");

		}
	}

	public void uploadAudio() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();
		File file = new File(filepath);
		mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		mpBuilder.addPart("file", new FileBody(file));
		HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
				+ "upload_SessionAudios.php");
		HttpClient httpClient = new DefaultHttpClient();
		httpPost_image.setEntity(mpBuilder.build());

		try {
			HttpResponse httpResponse = httpClient.execute(httpPost_image);

			String response = EntityUtils.toString(httpResponse.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void sendAudioNameToPhpFile() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost1 = new HttpPost(MainActivity.BASE_URL
				+ "upload_SessionAudioInfo.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.
			Log.d("31a", audioNameOnly);
			nameValuePairs.add(new BasicNameValuePair("audioname",
					audioNameOnly));

			httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost1);

		}/*
		 * catch (ClientProtocolException e) { // TODO Auto-generated catch
		 * block }
		 */catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
}
