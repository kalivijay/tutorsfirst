package com.example.tutorsfirst;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class PreTab3Activity extends Activity {

	RelativeLayout iv_add_new_session, iv_review_session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pre_tab3);

		iv_add_new_session = (RelativeLayout) findViewById(R.id.relAddSession);
		iv_review_session = (RelativeLayout) findViewById(R.id.relReview);

		iv_add_new_session.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent myIntent = new Intent(PreTab3Activity.this,
						TabHostActivity.class);

				/*
				 * Intent myIntent = new Intent(PreTab3Activity.this,
				 * TabHostActivity.class);
				 */

				TabHostActivity.mangae_tabs = "3";

				// Log.d("7a", "you clicked" + TabHostActivity.mangae_tabs);

				startActivity(myIntent);
			}
		});

		iv_review_session.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent myIntent = new Intent(PreTab3Activity.this,
						TabHostActivity.class);

				/*
				 * Intent myIntent = new Intent(PreTab3Activity.this,
				 * TabHostActivity.class);
				 */
				TabHostActivity.mangae_tabs = "4";
				Log.d("vk", "you clicked" + TabHostActivity.mangae_tabs);
				startActivity(myIntent);
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}
}
