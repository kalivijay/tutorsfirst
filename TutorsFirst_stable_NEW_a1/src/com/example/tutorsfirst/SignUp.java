package com.example.tutorsfirst;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SignUp extends Activity {

	EditText ed_name, ed_email, ed_phone, ed_password, ed_con_password,
			ed_last_name;

	Button btn_save;
	// TextView tv_select_image;
	public ProgressDialog pDialog;
	public ProgressDialog pDialog2;
	Bitmap image;
	String globelTime;

	// image uploading declaration
	File tempFile;
	String imageName = "one.png";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sign_up);

		initialize();

		image = BitmapFactory.decodeResource(getResources(),
				R.drawable.img_calresult_box);

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ed_name.getText().toString().length() == 0
						|| ed_email.getText().toString().length() == 0
						|| ed_phone.getText().toString().length() == 0
						|| ed_password.getText().toString().length() == 0
						|| ed_con_password.getText().toString().length() == 0) {

					// showing dialog
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							SignUp.this);
					dlgAlert.setMessage("Fill All the Fields");
					dlgAlert.setTitle("App Title");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();
				} else if (!ed_password.getText().toString()
						.equals(ed_con_password.getText().toString())) {

					// showing dialog
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							SignUp.this);
					dlgAlert.setMessage("Password Not Match !");
					dlgAlert.setTitle("App Title");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();

				}

				else {
					globelTime = getCurrentTimeStamp() + ".png";

					new GetNames().execute();

					// image uploadin code
					/*-------------------------*/

					CacheStorageUtil
							.saveToCache(SignUp.this, globelTime, image);
					Log.d("myjava", " hi");

					// -------------------------

					new sendImage().execute();
					Intent jump_login_screen = new Intent(SignUp.this,
							MainActivity.class);
					startActivity(jump_login_screen);
				}
			}
		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetNames extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(SignUp.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

		}
	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL + "addTutor.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.
			nameValuePairs.add(new BasicNameValuePair("name", ed_name.getText()
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("email", ed_email
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phone", ed_phone
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("password", ed_password
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("lastname", ed_last_name
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("userpic", globelTime));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

		}/*
		 * catch (ClientProtocolException e) { // TODO Auto-generated catch
		 * block }
		 */catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	public void initialize() {
		ed_name = (EditText) findViewById(R.id.etFirstName);
		ed_email = (EditText) findViewById(R.id.etEmail);
		ed_phone = (EditText) findViewById(R.id.etPhone);
		ed_password = (EditText) findViewById(R.id.etPassword);
		ed_con_password = (EditText) findViewById(R.id.etCPassword);

		ed_last_name = (EditText) findViewById(R.id.etLastName);

		btn_save = (Button) findViewById(R.id.btnSignUp);

	}

	/**
	 * 
	 * @return yyyy-MM-dd HH:mm:ss formate date as string
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getCurrentTimeStamp() {
		try {

			/*
			 * SimpleDateFormat dateFormat = new SimpleDateFormat(
			 * "yyyy-MM-dd HH:mm:ss");
			 */

			SimpleDateFormat take_date = new SimpleDateFormat("yyyyMMddHHmmss");
			String system_date = take_date.format(new Date());

			return system_date;
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	public void uploadImage() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();
		File file = new File(getCacheDir(), globelTime);
		mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		mpBuilder.addPart("file", new FileBody(file));
		HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
				+ "upload_ProfileImage.php");
		HttpClient httpClient = new DefaultHttpClient();
		httpPost_image.setEntity(mpBuilder.build());

		try {
			HttpResponse httpResponse = httpClient.execute(httpPost_image);

			String response = EntityUtils.toString(httpResponse.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// return null;
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class sendImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog2 = new ProgressDialog(SignUp.this);
			pDialog2.setMessage("Please wait...");
			pDialog2.setCancelable(false);
			pDialog2.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			uploadImage();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar
			Log.d("newtime", "upif");
			if (pDialog2.isShowing()) {
				pDialog2.dismiss();
				Log.d("newtime", "inif");

			}
			Log.d("newtime", "bottomif");

		}
	}

}
