package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class StudentData_with_Checkboxs extends Activity {

	// Layout Declaration
	TextView tv_add_student;
	TextView btn_save;
	ListView lv_tab2;
	ArrayList<ItemsTab2> tab2_arraylist = new ArrayList<ItemsTab2>();
	public ProgressDialog pDialog;

	// fetch data from json
	JSONArray usedForEdit;
	String stdFirstName;
	String guarFirstName;
	String guarLastName;
	String stdLastName;
	String studentID_send_to_model;
	MyAdapter adapter;

	// get data from json and send to edit activity after clicking on list
	// Variable declaration
	String studentname, guardianname, address1, address2, email1, email2,
			phone1, phone2, userpic, tutorID, studentID;
	// i make studentID static because it is uses in long click also.
	// static String studentID;

	int deletePosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.student_data_with_checkboxs);
		init();

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new MyAsyncTask().execute();
				Intent myIntent = new Intent(StudentData_with_Checkboxs.this,
						TabHostActivity.class);
				TabHostActivity.mangae_tabs = "3";
				startActivity(myIntent);
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		new MyAsyncTask().execute();

	}

	public void init() {
		tv_add_student = (TextView) findViewById(R.id.tv_add_student);
		lv_tab2 = (ListView) findViewById(R.id.lv_tab2);
		btn_save = (TextView) findViewById(R.id.btn_save);
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			tab2_arraylist.clear();

			// Showing progress dialog
			pDialog = new ProgressDialog(StudentData_with_Checkboxs.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			adapter = new MyAdapter(StudentData_with_Checkboxs.this, 0,
					tab2_arraylist, false);

			lv_tab2.setAdapter(adapter);

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "GetStudent.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			Log.d("28e", "tutid : " + Tab1Activity.tutorID);
			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

			Log.d("peh", "response : " + responseStr);

			// Getting JSON Array node
			JSONArray nameList = new JSONArray(responseStr);
			usedForEdit = new JSONArray(responseStr);

			// looping through All Names

			for (int i = 0; i < nameList.length(); i++) {
				JSONObject n = nameList.getJSONObject(i);
				/*
				 * studentName = n.getString("studentname"); guardianName =
				 * n.getString("guardianname"); email = n.getString("email1");
				 */
				stdFirstName = n.getString("studentname");
				stdLastName = n.getString("slastname");
				guarFirstName = n.getString("guardianname");
				guarLastName = n.getString("g1lname");
				studentID_send_to_model = n.getString("studentID");

				/*
				 * tab2_arraylist.add(new ItemsTab2(R.drawable.ic_launcher,
				 * studentName, guardianName, email));
				 */
				tab2_arraylist.add(new ItemsTab2(R.drawable.ic_launcher,
						stdFirstName, stdLastName, guarFirstName, guarLastName,
						studentID_send_to_model, false));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void AlertBox() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				StudentData_with_Checkboxs.this);

		// set title
		alertDialogBuilder.setTitle("Your Title");

		// set dialog message
		alertDialogBuilder
				.setMessage("Are you really want to delete!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								tab2_arraylist.remove(deletePosition);
								adapter.notifyDataSetChanged();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

}
