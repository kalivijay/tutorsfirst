package com.example.tutorsfirst;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Tab1Activity extends Activity {

	// Layout Declaration

	EditText ed_name, ed_email, ed_phone, ed_password, ed_last_name;
	Bitmap image;
	String imageURL;
	Button btn_save;
	public ProgressDialog pDialog;
	public ProgressDialog pDialog2;
	// these sring take the password comes from tabhost
	String password;
	// i make tutor id globel because i is requird in other tabs.
	public static String tutorID;
	// response string after send one_username and one_password.
	String responseStr;
	JSONArray arr;
	// these data to be set on edittexts
	String updated_email, updated_password, updated_name, updated_phone,
			updated_userpic, updated_tutorID, updated_lastName;

	TextView log_out_label;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab1);
		init();

		new getProfileData().execute();
		Intent getValues = getIntent();

		/*
		 * Log.d("71set", updated_email + ":   :" + updated_name + ":   :" +
		 * updated_password + updated_phone + ":   :" + updated_userpic +
		 * ":   :" + updated_lastName);
		 */

		// imageURL = getValues.getStringExtra("userpic");

		if (tutorID == null) {

			tutorID = getValues.getStringExtra("tutorID");
		}

		Log.d("28ab",
				"tutorID" + tutorID + "  name:  "
						+ getValues.getStringExtra("name"));

		// new AsynTask().execute();

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (ed_email.getText().toString()
						.matches(MainActivity.emailPattern)) {
					if (ed_name.getText().toString().length() == 0
							|| ed_last_name.getText().toString().length() == 0
							|| ed_email.getText().toString().length() == 0) {
						okButtonAlertbox();
					} else {
						new AsynTask().execute();
					}

				} else {
					// showing dialog
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							Tab1Activity.this);
					dlgAlert.setMessage("Invalid Email Address");
					dlgAlert.setTitle("App Title");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();
				}

				// jump to login screen
				/*
				 * Intent jump_login_screen = new Intent(Tab1Activity.this,
				 * MainActivity.class); startActivity(jump_login_screen);
				 */
			}
		});

		log_out_label.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertBox();
			}
		});

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class AsynTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab1Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

			// iv_user_pic_tab1.setImageBitmap(image);

		}
	}

	public void init() {

		ed_name = (EditText) findViewById(R.id.etFirstName);
		ed_phone = (EditText) findViewById(R.id.etPhone);
		ed_email = (EditText) findViewById(R.id.etEmail);
		btn_save = (Button) findViewById(R.id.btnSave);
		log_out_label = (TextView) findViewById(R.id.tvLogOut);
		ed_password = (EditText) findViewById(R.id.etPassword);
		ed_last_name = (EditText) findViewById(R.id.etLastName);
		SpannableString content = new SpannableString("Tap here to Log Out");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		log_out_label.setText(content);
	}

	// this methods Actually send data and get Response as JSON.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "editTutor.php");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// these is SAME as define in
			// php file.

			nameValuePairs.add(new BasicNameValuePair("email", ed_email
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("name", ed_name.getText()
					.toString()));

			nameValuePairs.add(new BasicNameValuePair("lastname", ed_last_name
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("phone", ed_phone
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("password", ed_password
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("tutorID", tutorID));
			// nameValuePairs.add(new BasicNameValuePair("userpic", imageURL));

			Log.d("28b", ed_email.getText().toString() + " : "
					+ ed_name.getText().toString() + " : "
					+ ed_phone.getText().toString() + " : " + password + " : "
					+ tutorID + " : " + imageURL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			Log.d("NewResponse", "start:  " + response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

	}

	public void AlertBox() {

		Log.d("6b", "come in alertbox");

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Tab1Activity.this);

		// set title
		alertDialogBuilder.setTitle("Are You Sure ?");

		// set dialog message
		alertDialogBuilder
				.setMessage("Are you sure want to logout !")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								Intent myIntent = new Intent(Tab1Activity.this,
										MainActivity.class);
								startActivity(myIntent);
								finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class getProfileData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab1Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			getProfileData_method();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// it is required to put these statements in onPosteExecute you know
			// why !

			// if (arr.length() != 0) {
			// jump_tab1 = new Intent(MainActivity.this,
			// Tab1Activity.class);
			/*
			 * jump_tab1 = new Intent(MainActivity.this, TabHostActivity.class);
			 * jump_tab1.putExtra("email", email); jump_tab1.putExtra("name",
			 * name); jump_tab1.putExtra("password", password);
			 * jump_tab1.putExtra("phone", phone);
			 * jump_tab1.putExtra("last_name", lastName);
			 * jump_tab1.putExtra("userpic", userpic);
			 * jump_tab1.putExtra("tutorID", tutorID);
			 */
			// startActivity(jump_tab1);
			// }

			Log.d("72", updated_password);

			ed_email.setText(updated_email);
			ed_name.setText(updated_name);
			ed_phone.setText(updated_phone);
			ed_password.setText(updated_password);
			ed_last_name.setText(updated_lastName);

			password = updated_password;

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data and get Response as JSON.
	public void getProfileData_method() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "GetProfileData.php");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			Log.d("71", "one_user: " + MainActivity.one_username + "one_pass"
					+ MainActivity.one_password);

			nameValuePairs.add(new BasicNameValuePair("email",
					MainActivity.one_username));

			nameValuePairs.add(new BasicNameValuePair("password",
					MainActivity.one_password));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			Log.d("NewResponse", "start:  " + response);

			// now i convert the respone in responseStr

			responseStr = EntityUtils.toString(response.getEntity());

			Log.d("NewResponse", "start:  " + responseStr);

			// i above line i can see appropriate JSON.

			// now i get all json items saperately

			arr = new JSONArray(responseStr);

			Log.d("71", arr + "");

			if (arr.length() != 0) {
				Log.d("javavk", arr + "");

				JSONObject jObj = arr.getJSONObject(0);

				updated_email = jObj.getString("email");
				updated_name = jObj.getString("name");
				updated_password = jObj.getString("password");
				updated_phone = jObj.getString("phone");
				updated_userpic = jObj.getString("userpic");
				updated_lastName = jObj.getString("lastname");
				updated_tutorID = jObj.getString("tutorID");

				tutorID = updated_tutorID;

				Log.d("71", updated_email + ":   :" + updated_name + ":   :"
						+ updated_password + updated_phone + ":   :"
						+ updated_userpic + ":   :" + updated_lastName);

			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void okButtonAlertbox() {
		Builder alert = new AlertDialog.Builder(Tab1Activity.this);

		alert.setTitle("Fill Required Fields");
		alert.setMessage("Please fill all mandatory fiedls");
		alert.setPositiveButton("OK", null);
		alert.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}

}
