package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class Tab2Activity extends Activity {

	// Layout Declaration
	RelativeLayout tv_add_student;
	ListView lv_tab2;
	ArrayList<ItemsTab2> tab2_arraylist = new ArrayList<ItemsTab2>();
	public ProgressDialog pDialog;

	// fetch data from json
	JSONArray usedForEdit;
	String stdFirstName;
	String guarFirstName;
	String guarLastName;
	String stdLastName;
	MyAdapter adapter;

	static int item_position;
	static String studentID_again;

	// get data from json and send to edit activity after clicking on list
	// Variable declaration
	String studentname, guardianname, address1, address2, email1, email2,
			phone1, phone2, userpic, tutorID, studentID, home1, home2, work1,
			work2, studentLastName, guar1LastName, guar2FirstName,
			guar2LastName;
	// i make studentID static because it is uses in long click also.
	// static String studentID;

	int deletePosition;

	// info objects
	static String Tab2_edit_std_info_object;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab2);
		init();

		lv_tab2.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Log.d("29b", usedForEdit + "");
				try {
					Log.d("vk8a", usedForEdit.get(arg2) + "");
					JSONObject n2 = usedForEdit.getJSONObject(arg2);
					studentID = n2.getString("studentID");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				new AsyncTask_ForDeleteRecord().execute();
				deletePosition = arg2;
				AlertBox();

				return true;
			}
		});

		lv_tab2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				item_position = position;
				showSelectImageDialog();

			}
		});

		tv_add_student.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// jump to the edit student info

				Intent jump_Tab2_Enter_Std_Info = new Intent(Tab2Activity.this,
						TabHostActivity.class);

				/*
				 * Intent jump_Tab2_Enter_Std_Info = new
				 * Intent(Tab2Activity.this, TabHostActivity.class);
				 */
				TabHostActivity.mangae_tabs = "1";

				Log.d("ipod", "basic");
				startActivity(jump_Tab2_Enter_Std_Info);

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		/*
		 * new MyAsyncTask().execute(); adapter.notifyDataSetChanged();
		 */

		new MyAsyncTask().execute();

	}

	public void init() {
		tv_add_student = (RelativeLayout) findViewById(R.id.relAddStudent);
		lv_tab2 = (ListView) findViewById(R.id.lv_tab2);
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			tab2_arraylist.clear();

			// Showing progress dialog
			pDialog = new ProgressDialog(Tab2Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			adapter = new MyAdapter(Tab2Activity.this, 0, tab2_arraylist, true);

			lv_tab2.setAdapter(adapter);

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "GetStudent.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			Log.d("6c", "tutid : " + Tab1Activity.tutorID);

			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

			Log.d("7c", "response : " + responseStr);

			// Getting JSON Array node
			JSONArray nameList = new JSONArray(responseStr);
			usedForEdit = new JSONArray(responseStr);

			// looping through All Names

			for (int i = 0; i < nameList.length(); i++) {
				JSONObject n = nameList.getJSONObject(i);

				stdFirstName = n.getString("studentname");
				stdLastName = n.getString("slastname");
				guarFirstName = n.getString("guardianname");
				guarLastName = n.getString("g1lname");

				// Log.d("one", studentName + " : " + guardianName + " : " +
				// email);
				tab2_arraylist.add(new ItemsTab2(R.drawable.ic_launcher,
						stdFirstName, stdLastName, guarFirstName, guarLastName,
						"peh", false));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void AlertBox() {

		Log.d("6b", "come in alertbox");

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Tab2Activity.this);

		// set title
		alertDialogBuilder.setTitle("Your Title");

		// set dialog message
		alertDialogBuilder
				.setMessage("Are you really want to delete!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								tab2_arraylist.remove(deletePosition);
								Log.d("6b", "delete it");
								adapter.notifyDataSetChanged();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class AsyncTask_ForDeleteRecord extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// Showing progress dialog
			pDialog = new ProgressDialog(Tab2Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData_forDeleteRecord();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData_forDeleteRecord() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "deleteStudent.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			Log.d("29b", "studentid:  " + studentID);

			nameValuePairs.add(new BasicNameValuePair("studentID", studentID));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	private void showSelectImageDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(Tab2Activity.this);
		// builder.setTitle("Select Image");

		builder.setItems(getResources().getStringArray(R.array.student_tab),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						int pos = (int) Math.round(item);

						// To open the gallery
						if (pos == 0) {

							try {
								JSONObject n3 = usedForEdit
										.getJSONObject(item_position);
								studentID_again = n3.getString("studentID");
								Log.d("tagit", studentID_again);
								/*
								 * Intent jump_Tab3Activity = new Intent(
								 * Tab2Activity.this, Tab3Activity.class);
								 * jump_Tab3Activity.putExtra("this_student_id",
								 * studentID_again);
								 */

								Intent jump_Tab3Activity = new Intent(
										Tab2Activity.this,
										TabHostActivity.class);

								TabHostActivity.mangae_tabs = "4";

								startActivity(jump_Tab3Activity);
								/*
								 * new MyAsyncTask_for_get_session_of_this_std()
								 * .execute();
								 */
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else if (pos == 1) {
							editStudent(item_position);
						} else if (pos == 2) {
							dialog.dismiss();
						}

						dialog.dismiss();
					}
				});

		AlertDialog alertBox = builder.create();
		alertBox.show();
	}

	public void editStudent(int position) {
		Log.d("29a", usedForEdit + "");
		try {

			Log.d("29a", usedForEdit.get(position) + "");

			// usedForEdit is a JSONarray. and i cut a single jsonObject
			// from JSONarray and then fetch elements from that
			// jsonObject.

			JSONObject n1 = usedForEdit.getJSONObject(position);

			studentname = n1.getString("studentname");
			guardianname = n1.getString("guardianname");
			address1 = n1.getString("address1");
			address2 = n1.getString("address2");
			email1 = n1.getString("email1");
			email2 = n1.getString("email2");
			phone1 = n1.getString("phone1");
			phone2 = n1.getString("phone2");
			userpic = n1.getString("userpic");
			tutorID = n1.getString("tutorID");
			studentID = n1.getString("studentID");
			home1 = n1.getString("home1");
			home2 = n1.getString("home2");
			work1 = n1.getString("work1");
			work2 = n1.getString("work2");

			studentLastName = n1.getString("slastname");
			guar1LastName = n1.getString("g1lname");
			guar2FirstName = n1.getString("guardian2name");
			guar2LastName = n1.getString("g2lname");

			Log.d("vk29a", "second data" + studentname + " : " + guardianname
					+ " : " + address1 + " : " + address2 + " : " + email1
					+ " : " + email2 + " : " + phone1 + " : " + phone2 + " : "
					+ userpic + " : " + tutorID + " stdio_id : " + studentID
					+ " : ");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// jump to the edit student info
		/*
		 * Intent jump_Tab2_Edit_Std_Info = new Intent(Tab2Activity.this,
		 * Tab2_Edit_Std_Info.class);
		 */

		Intent jump_Tab2_Edit_Std_Info = new Intent(Tab2Activity.this,
				TabHostActivity.class);

		// TabHostActivity.mangae_tabs = "5";

		jump_Tab2_Edit_Std_Info.putExtra("studentname", studentname);
		jump_Tab2_Edit_Std_Info.putExtra("stdLastName", stdLastName);
		jump_Tab2_Edit_Std_Info.putExtra("guardianname", guardianname);
		jump_Tab2_Edit_Std_Info.putExtra("address1", address1);
		jump_Tab2_Edit_Std_Info.putExtra("address2", address2);
		jump_Tab2_Edit_Std_Info.putExtra("email1", email1);
		jump_Tab2_Edit_Std_Info.putExtra("email2", email2);
		jump_Tab2_Edit_Std_Info.putExtra("phone1", phone1);
		jump_Tab2_Edit_Std_Info.putExtra("phone2", phone2);
		jump_Tab2_Edit_Std_Info.putExtra("userpic", userpic);
		jump_Tab2_Edit_Std_Info.putExtra("studentID", studentID);

		jump_Tab2_Edit_Std_Info.putExtra("home1", home1);
		jump_Tab2_Edit_Std_Info.putExtra("work1", work1);
		jump_Tab2_Edit_Std_Info.putExtra("home2", home2);
		jump_Tab2_Edit_Std_Info.putExtra("work2", work2);

		jump_Tab2_Edit_Std_Info.putExtra("studentLastName", studentLastName);
		jump_Tab2_Edit_Std_Info.putExtra("guar1LastName", guar1LastName);
		jump_Tab2_Edit_Std_Info.putExtra("guar2FirstName", guar2FirstName);
		jump_Tab2_Edit_Std_Info.putExtra("guar2LastName", guar2LastName);

		TabHostActivity.mangae_tabs = "5";

		// new way ------------------------

		JSONObject n1_same;
		try {
			n1_same = usedForEdit.getJSONObject(position);

			/*
			 * jump_Tab2_Edit_Std_Info.putExtra("json_from_tab2activity",
			 * n1_same.toString());
			 */
			Tab2_edit_std_info_object = n1_same.toString();
			// Log.d("vkone", n1_same.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		startActivity(jump_Tab2_Edit_Std_Info);

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class MyAsyncTask_for_get_session_of_this_std extends
			AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// tab2_arraylist.clear();

			// Showing progress dialog
			pDialog = new ProgressDialog(Tab2Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData_for_get_session_of_this_std();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// adapter = new MyAdapter(Tab2Activity.this, 0, tab2_arraylist,
			// true);

			// lv_tab2.setAdapter(adapter);

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData_for_get_session_of_this_std() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "ReviewStudent.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			// Log.d("6c", "tutid : " + Tab1Activity.tutorID);

			nameValuePairs.add(new BasicNameValuePair("selectstudent",
					studentID_again));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

			Log.d("nowfinal", "response : " + responseStr);

			// Getting JSON Array node
			JSONArray nameList = new JSONArray(responseStr);
			usedForEdit = new JSONArray(responseStr);

			// looping through All Names

			/*
			 * for (int i = 0; i < nameList.length(); i++) { JSONObject n =
			 * nameList.getJSONObject(i);
			 * 
			 * stdFirstName = n.getString("studentname"); stdLastName =
			 * n.getString("slastname"); guarFirstName =
			 * n.getString("guardianname"); guarLastName =
			 * n.getString("g1lname");
			 * 
			 * // Log.d("one", studentName + " : " + guardianName + " : " + //
			 * email); tab2_arraylist.add(new ItemsTab2(R.drawable.ic_launcher,
			 * stdFirstName, stdLastName, guarFirstName, guarLastName, "peh",
			 * false)); }
			 */

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
