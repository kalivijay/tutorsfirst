package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Tab2_Edit_Std_Info extends Activity implements OnClickListener {

	// Layout Declaration
	// Button btn_sign_up, btn_login;

	EditText ed_name, ed_guardianname, ed_Address1, ed_Address2, ed_phone1,
			ed_phone2, ed_email1, ed_email2, ed_home1, ed_work1,
			ed_guar2_home1, ed_guar2_work1, ed_last_name,
			ed_Guardian_last_name, ed_Guardian2_name, ed_Guardian2_last_name;
	Button btn_save;
	// variables

	String tutorID, studentID;

	// ProgressDialog
	public ProgressDialog pDialog;

	// call image declaration
	ImageView iv_call_1, iv_call_2, iv_call_3, iv_call_4, iv_call_5, iv_call_6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tab2_edit_std_info);
		init();

		// for calling only
		iv_call_1.setOnClickListener(this);
		iv_call_2.setOnClickListener(this);
		iv_call_3.setOnClickListener(this);
		iv_call_4.setOnClickListener(this);
		iv_call_5.setOnClickListener(this);
		iv_call_6.setOnClickListener(this);
		// ------------------
		// below lines are useless because i use a static string which a json
		// object value.
		Intent myIntent = getIntent();// stdLastName,
		ed_name.setText(myIntent.getStringExtra("studentname"));
		ed_guardianname.setText(myIntent.getStringExtra("guardianname"));
		ed_Address1.setText(myIntent.getStringExtra("address1"));
		ed_Address2.setText(myIntent.getStringExtra("address2"));
		ed_phone1.setText(myIntent.getStringExtra("phone1"));
		ed_phone2.setText(myIntent.getStringExtra("phone2"));
		ed_email1.setText(myIntent.getStringExtra("email1"));
		ed_email2.setText(myIntent.getStringExtra("email2"));
		tutorID = myIntent.getStringExtra("tutorID");
		studentID = myIntent.getStringExtra("studentID");

		ed_home1.setText(myIntent.getStringExtra("home1"));
		ed_work1.setText(myIntent.getStringExtra("work1"));
		ed_guar2_home1.setText(myIntent.getStringExtra("home2"));
		ed_guar2_work1.setText(myIntent.getStringExtra("work2"));

		ed_last_name.setText(myIntent.getStringExtra("studentLastName"));
		ed_Guardian_last_name.setText(myIntent.getStringExtra("guar1LastName"));
		ed_Guardian2_name.setText(myIntent.getStringExtra("guar2FirstName"));
		ed_Guardian2_last_name
				.setText(myIntent.getStringExtra("guar2LastName"));
		/*
		 * try { JSONObject obj = new JSONObject(myIntent.getStringExtra(
		 * "json_from_tab2activity")); Log.d("vkone", obj.toString());
		 * Log.d("13a", "adfasdfjasdj"+obj.getString("studentname") + " : " +
		 * obj.getString("guardianname")); } catch (JSONException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		try {
			Log.d("testmath", "hi:  " + Tab2Activity.Tab2_edit_std_info_object);

			JSONObject Tab2_edit_std_info_object = new JSONObject(
					Tab2Activity.Tab2_edit_std_info_object);
			Log.d("testmath",
					Tab2_edit_std_info_object.getString("studentname") + " : "
							+ Tab2_edit_std_info_object.getString("slastname"));

			// taking value from object and setting them
			
			ed_name.setText(Tab2_edit_std_info_object.getString("studentname"));
			ed_guardianname.setText(Tab2_edit_std_info_object
					.getString("guardianname"));
			ed_Address1
					.setText(Tab2_edit_std_info_object.getString("address1"));
			ed_Address2
					.setText(Tab2_edit_std_info_object.getString("address2"));
			ed_phone1.setText(Tab2_edit_std_info_object.getString("phone1"));
			ed_phone2.setText(Tab2_edit_std_info_object.getString("phone2"));
			ed_email1.setText(Tab2_edit_std_info_object.getString("email1"));
			ed_email2.setText(Tab2_edit_std_info_object.getString("email2"));
			ed_home1.setText(Tab2_edit_std_info_object.getString("home1"));
			ed_work1.setText(Tab2_edit_std_info_object.getString("work1"));
			ed_guar2_home1
					.setText(Tab2_edit_std_info_object.getString("home2"));
			ed_guar2_work1
					.setText(Tab2_edit_std_info_object.getString("work2"));
			ed_last_name.setText(Tab2_edit_std_info_object
					.getString("slastname"));
			ed_Guardian_last_name.setText(Tab2_edit_std_info_object
					.getString("g1lname"));
			ed_Guardian2_name.setText(Tab2_edit_std_info_object
					.getString("guardian2name"));
			ed_Guardian2_last_name.setText(Tab2_edit_std_info_object
					.getString("g2lname"));
			studentID = Tab2_edit_std_info_object.getString("studentID");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				/*
				 * Intent myIntent = new Intent(Tab2_Edit_Std_Info.this,
				 * Tab2Activity.class);
				 */

				if (!(ed_name.getText().toString().length() == 0
						|| ed_last_name.getText().toString().length() == 0
						|| ed_guardianname.getText().toString().length() == 0
						|| ed_Guardian_last_name.getText().toString().length() == 0 || ed_email1
						.getText().toString().length() == 0)) {
					if (ed_email1.getText().toString()
							.matches(MainActivity.emailPattern)) {
						new AsynTask().execute();

						Intent jump_Tab2Activity = new Intent(
								Tab2_Edit_Std_Info.this, TabHostActivity.class);
						 TabHostActivity.mangae_tabs = "tab2shown";
						startActivity(jump_Tab2Activity);
					} else {
						AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
								Tab2_Edit_Std_Info.this);
						dlgAlert.setMessage("Invalid Email Address");
						dlgAlert.setTitle("App Title");
						dlgAlert.setPositiveButton("OK", null);
						dlgAlert.setCancelable(true);
						dlgAlert.create().show();
					}

				} else {
					okButtonAlertbox();

				}
			}
		});

	}

	public void init() {

		// Edittexts
		ed_name = (EditText) findViewById(R.id.etFirstName);
		ed_guardianname = (EditText) findViewById(R.id.etGFirstName);
		ed_Address1 = (EditText) findViewById(R.id.etGAddress);
		ed_Address2 = (EditText) findViewById(R.id.etGAddress_2);
		ed_phone1 = (EditText) findViewById(R.id.etGMobile);
		ed_phone2 = (EditText) findViewById(R.id.etGMobile_2);
		ed_email1 = (EditText) findViewById(R.id.etGEmail);
		ed_email2 = (EditText) findViewById(R.id.etGEmail_2);
		// button
		btn_save = (Button) findViewById(R.id.btnSave);

		ed_home1 = (EditText) findViewById(R.id.etGHome);
		ed_work1 = (EditText) findViewById(R.id.etGWork);

		ed_guar2_home1 = (EditText) findViewById(R.id.etGHome_2);
		ed_guar2_work1 = (EditText) findViewById(R.id.etGWork_2);

		ed_last_name = (EditText) findViewById(R.id.etLastName);

		ed_Guardian_last_name = (EditText) findViewById(R.id.etGLastName);
		ed_Guardian2_name = (EditText) findViewById(R.id.etGFirstName_2);
		ed_Guardian2_last_name = (EditText) findViewById(R.id.etGLastName_2);

		iv_call_1 = (ImageView) findViewById(R.id.ivGMobile);
		iv_call_2 = (ImageView) findViewById(R.id.ivGHome);
		iv_call_3 = (ImageView) findViewById(R.id.ivGWork);
		iv_call_4 = (ImageView) findViewById(R.id.ivGMobile_2);
		iv_call_5 = (ImageView) findViewById(R.id.ivGHome_2);
		iv_call_6 = (ImageView) findViewById(R.id.ivGWork_2);

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class AsynTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab2_Edit_Std_Info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			// showImageFromURL();
			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

			// iv_user_pic_tab1.setImageBitmap(image);

		}
	}

	// this methods Actually send data and get Response as JSON.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "editStudent.php");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// these is SAME as define in
			// php file.

			nameValuePairs.add(new BasicNameValuePair("studentname", ed_name
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("slastname", ed_last_name
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("guardianname",
					ed_guardianname.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("g1lname",
					ed_Guardian_last_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("guardian2name",
					ed_Guardian2_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("g2lname",
					ed_Guardian2_last_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("address1", ed_Address1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("address2", ed_Address2
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phone1", ed_phone1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phone2", ed_phone2
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("email1", ed_email1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("email2", ed_email2
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));
			nameValuePairs.add(new BasicNameValuePair("studentID", studentID));

			nameValuePairs.add(new BasicNameValuePair("home1", ed_home1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("work1", ed_work1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("home2", ed_guar2_home1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("work2", ed_guar2_work1
					.getText().toString()));

			// nameValuePairs.add(new BasicNameValuePair("userpic", imageURL));

			/*
			 * Log.d("28b", ed_email.getText().toString() + " : " +
			 * ed_name.getText().toString() + " : " +
			 * ed_phone.getText().toString() + " : " + password + " : " +
			 * tutorID + " : " + imageURL);
			 */
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			Log.d("NewResponse", "start:  " + response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		// calling
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		switch (arg0.getId()) {

		case R.id.ivGMobile:

			callIntent.setData(Uri.parse("tel:"
					+ ed_phone1.getText().toString().toString()));

			break;
		case R.id.ivGHome:
			callIntent.setData(Uri.parse("tel:"
					+ ed_home1.getText().toString().toString()));

			break;
		case R.id.ivGWork:
			callIntent.setData(Uri.parse("tel:"
					+ ed_work1.getText().toString().toString()));

			break;
		case R.id.ivGHome_2:
			callIntent.setData(Uri.parse("tel:"
					+ ed_guar2_home1.getText().toString().toString()));

			break;
		case R.id.ivGWork_2:
			callIntent.setData(Uri.parse("tel:"
					+ ed_guar2_work1.getText().toString().toString()));

			break;
		case R.id.ivGMobile_2:
			callIntent.setData(Uri.parse("tel:"
					+ ed_phone1.getText().toString().toString()));

			break;

		}
		startActivity(callIntent);
	}

	public void okButtonAlertbox() {
		Builder alert = new AlertDialog.Builder(Tab2_Edit_Std_Info.this);

		alert.setTitle("Fill Required Fields");
		alert.setMessage("Please fill all mandatory fiedls");
		alert.setPositiveButton("OK", null);
		alert.show();
	}
}
