package com.example.tutorsfirst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Tab2_Enter_stud_info extends Activity {

	// Layout Declaration
	// TextView tv_select_image;
	EditText ed_name, ed_Guardian_name, ed_Address1, ed_Address2, ed_phone1,
			ed_phone2, ed_email1, ed_email2, ed_last_name,
			ed_Guardian_last_name, ed_Guardian2_name, ed_Guardian2_last_name,
			ed_home1, ed_work1, ed_guar2_home1, ed_guar2_work1;

	Button btn_save;
	String globelTime_tab2;
	public ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tab2_enter_std_info);
		init();

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*
				 * Intent jump_Tab2Activity = new Intent(
				 * Tab2_Enter_stud_info.this, Tab2Activity.class);
				 */
				/*
				 * if (ed_email1.getText().toString()
				 * .matches(MainActivity.emailPattern)) { if
				 * (ed_name.getText().toString().length() == 0 ||
				 * ed_last_name.getText().toString().length() == 0 ||
				 * ed_Guardian_name.getText().toString().length() == 0 ||
				 * ed_Guardian_last_name.getText().toString() .length() == 0 ||
				 * ed_email1.getText().toString().length() == 0) {
				 * okButtonAlertbox(); } else {
				 * 
				 * globelTime_tab2 = SignUp.getCurrentTimeStamp() + ".png";
				 * 
				 * new SaveData().execute();
				 * 
				 * Intent jump_Tab2Activity = new Intent(
				 * Tab2_Enter_stud_info.this, Tab2Activity.class); //
				 * TabHostActivity.mangae_tabs = "2";
				 * startActivity(jump_Tab2Activity); } } else { // showing
				 * dialog AlertDialog.Builder dlgAlert = new
				 * AlertDialog.Builder( Tab2_Enter_stud_info.this);
				 * dlgAlert.setMessage("Invalid Email Address");
				 * dlgAlert.setTitle("App Title");
				 * dlgAlert.setPositiveButton("OK", null);
				 * dlgAlert.setCancelable(true); dlgAlert.create().show();
				 * 
				 * }
				 */
				if (!(ed_name.getText().toString().length() == 0
						|| ed_last_name.getText().toString().length() == 0
						|| ed_Guardian_name.getText().toString().length() == 0
						|| ed_Guardian_last_name.getText().toString().length() == 0
						|| ed_email1.getText().toString().length() == 0)) {

					if (ed_email1.getText().toString()
							.matches(MainActivity.emailPattern)) {
						globelTime_tab2 = SignUp.getCurrentTimeStamp() + ".png";
						new SaveData().execute();
						Intent jump_Tab2Activity = new Intent(
								Tab2_Enter_stud_info.this, TabHostActivity.class);
						TabHostActivity.mangae_tabs = "tab2shown";
						Log.d("justme", "justme");
						startActivity(jump_Tab2Activity);
					} else {
						AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
								Tab2_Enter_stud_info.this);
						dlgAlert.setMessage("Invalid Email Address");
						dlgAlert.setTitle("App Title");
						dlgAlert.setPositiveButton("OK", null);
						dlgAlert.setCancelable(true);
						dlgAlert.create().show();
					}
				} else {
					okButtonAlertbox();
				}

			}
		});

	}

	public void init() {

		ed_name = (EditText) findViewById(R.id.etFirstName);
		ed_last_name = (EditText) findViewById(R.id.etLastName);
		ed_Guardian_name = (EditText) findViewById(R.id.etGFirstName);
		ed_Guardian_last_name = (EditText) findViewById(R.id.etGLastName);
		ed_Guardian2_name = (EditText) findViewById(R.id.etGFirstName_2);
		ed_Address1 = (EditText) findViewById(R.id.etGAddress);
		ed_Address2 = (EditText) findViewById(R.id.etGAddress_2);
		ed_phone1 = (EditText) findViewById(R.id.etGMobile);
		ed_phone2 = (EditText) findViewById(R.id.etGMobile_2);
		ed_email1 = (EditText) findViewById(R.id.etGEmail);
		ed_email2 = (EditText) findViewById(R.id.etGEmail_2);
		ed_home1 = (EditText) findViewById(R.id.etGHome);
		ed_work1 = (EditText) findViewById(R.id.etGWork);

		ed_guar2_home1 = (EditText) findViewById(R.id.etGHome_2);
		ed_guar2_work1 = (EditText) findViewById(R.id.etGWork_2);

		btn_save = (Button) findViewById(R.id.btnSave);
		ed_Guardian2_last_name = (EditText) findViewById(R.id.etGLastName_2);

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SaveData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab2_Enter_stud_info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

		}
	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "addStudent.php");

		try {

			Log.d("6a", ed_home1.getText().toString() + " : "
					+ ed_work1.getText().toString() + " : "
					+ ed_guar2_home1.getText().toString() + " : "
					+ ed_guar2_work1.getText().toString());

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.
			nameValuePairs.add(new BasicNameValuePair("studentname", ed_name
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("slastname", ed_last_name
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("guardianname",
					ed_Guardian_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("guardian2name",
					ed_Guardian2_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("g1lname",
					ed_Guardian_last_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("g2lname",
					ed_Guardian2_last_name.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("address1", ed_Address1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("address2", ed_Address2
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phone1", ed_phone1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phone2", ed_phone2
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("email1", ed_email1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("email2", ed_email2
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("home1", ed_home1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("work1", ed_work1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("home2", ed_guar2_home1
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("work2", ed_guar2_work1
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));

			Log.d("28d", ed_name.getText().toString() + " : "
					+ Tab1Activity.tutorID);

			nameValuePairs.add(new BasicNameValuePair("userpic",
					globelTime_tab2));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

		}/*
		 * catch (ClientProtocolException e) { // TODO Auto-generated catch
		 * block }
		 */catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	/*
	 * public void fillRequiredFields() { // ed_name.get if
	 * (ed_name.getText().toString().length() == 0 ||
	 * ed_last_name.getText().toString().length() == 0 ||
	 * ed_Guardian_name.getText().toString().length() == 0 ||
	 * ed_Guardian_last_name.getText().toString().length() == 0 ||
	 * ed_email1.getText().toString().length() == 0) { okButtonAlertbox(); } }
	 */

	public void okButtonAlertbox() {
		Builder alert = new AlertDialog.Builder(Tab2_Enter_stud_info.this);

		alert.setTitle("Fill Required Fields");
		alert.setMessage("Please fill all mandatory fiedls");
		alert.setPositiveButton("OK", null);
		alert.show();
	}
}
