package com.example.tutorsfirst;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class Tab3Activity extends Activity {

	// Layout Declaration

	ListView lv_tab3;
	ArrayList<ItemsTab3> tab3_arraylist = new ArrayList<ItemsTab3>();
	MyAdapter_Tab3 adapter;
	public ProgressDialog pDialog;

	// fetch data from json (these are the things to show on list)
	JSONArray usedForEdit;
	String Student_name_to_show_on_list, Student_last_name_to_show_on_list,
			Session_to_show_on_list, Subject_to_show_on_list,
			SessionDate_to_show_on_list, StartTime_to_show_on_list,
			EndTime_to_show_on_list;

	// get data from json and send to edit activity after clicking on list
	// Variable declaration
	String selectsubject, session, outoftotal, starttime, endtime,
			sessionduration, nextsessiondate, nextsessiontime, sessionnotes,
			signature;
	String sessionID;

	int deletePosition;

	// info objects
	static String Tab3_edit_std_info_object;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab3);
		init();

		Log.d("29d", "oncreate");

		lv_tab3.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub

				Log.d("29d", "just up");

				try {

					Log.d("29d", "values");

					JSONObject n1 = usedForEdit.getJSONObject(position);

					Log.d("29d", usedForEdit.getJSONObject(position) + "");

					selectsubject = n1.getString("selectsubject");
					session = n1.getString("session");
					outoftotal = n1.getString("outoftotal");
					starttime = n1.getString("starttime");
					endtime = n1.getString("endtime");
					sessionduration = n1.getString("sessionduration");
					nextsessiondate = n1.getString("nextsessiondate");
					nextsessiontime = n1.getString("nextsessiontime");
					sessionnotes = n1.getString("sessionnotes");
					signature = n1.getString("signature");

					sessionID = n1.getString("sessionID");
					Log.d("29d", selectsubject + " : " + session);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// jump to the edit session info
				Intent jump_Tab3_Edit_Session_Info = new Intent(
						Tab3Activity.this, TabHostActivity.class);

				TabHostActivity.mangae_tabs = "6";

				jump_Tab3_Edit_Session_Info.putExtra("selectsubject",
						selectsubject);
				jump_Tab3_Edit_Session_Info.putExtra("session", session);
				jump_Tab3_Edit_Session_Info.putExtra("outoftotal", outoftotal);
				jump_Tab3_Edit_Session_Info.putExtra("starttime", starttime);
				jump_Tab3_Edit_Session_Info.putExtra("endtime", endtime);
				jump_Tab3_Edit_Session_Info.putExtra("sessionduration",
						sessionduration);
				jump_Tab3_Edit_Session_Info.putExtra("nextsessiondate",
						nextsessiondate);
				jump_Tab3_Edit_Session_Info.putExtra("nextsessiontime",
						nextsessiontime);
				jump_Tab3_Edit_Session_Info.putExtra("sessionnotes",
						sessionnotes);
				jump_Tab3_Edit_Session_Info.putExtra("signature", signature);

				jump_Tab3_Edit_Session_Info.putExtra("sessionID", sessionID);

				// ------------------------------------

				// NEW LOGICS STARTS HERE... ABOVE IS USELESS

				JSONObject n1_same;

				try {
					n1_same = usedForEdit.getJSONObject(position);

					Tab3_edit_std_info_object = n1_same.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				startActivity(jump_Tab3_Edit_Session_Info);

			}
		});

		lv_tab3.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				try {
					Log.d("29b", usedForEdit.get(arg2) + "");
					JSONObject n2 = usedForEdit.getJSONObject(arg2);
					sessionID = n2.getString("sessionID");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				new AsyncTask_ForDeleteRecord().execute();
				deletePosition = arg2;
				AlertBox();
				return true;
			}
		});

	}

	public void init() {
		lv_tab3 = (ListView) findViewById(R.id.lv_tab3);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new MyAsyncTask().execute();

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			tab3_arraylist.clear();

			// Showing progress dialog
			pDialog = new ProgressDialog(Tab3Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			adapter = new MyAdapter_Tab3(Tab3Activity.this, 0, tab3_arraylist);

			lv_tab3.setAdapter(adapter);

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}
			Log.d("14a", usedForEdit.length() + "");
			if (usedForEdit.length() == 0) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						Tab3Activity.this);
				alert.setTitle("No Sessions Found!");
				alert.setMessage("There are no submitted sessions.");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						});
				alert.show();
			}

		}

	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header

		Log.d("go", "in send data");

		HttpClient httpclient = new DefaultHttpClient();

		Intent take_values = getIntent();

		Log.d("finaldata",
				"value" + take_values.getStringExtra("this_student_id"));

		HttpPost httppost;

		/*
		 * if (take_values.getStringExtra("this_student_id") != null) { httppost
		 * = new HttpPost(MainActivity.BASE_URL + "ReviewStudent.php"); } else {
		 * httppost = new HttpPost(MainActivity.BASE_URL + "GetSession.php"); }
		 */

		Log.d("14a", "value" + Tab2Activity.studentID_again);

		if (Tab2Activity.studentID_again != null) {
			httppost = new HttpPost(MainActivity.BASE_URL + "ReviewStudent.php");
		} else {
			httppost = new HttpPost(MainActivity.BASE_URL + "GetSession.php");
		}

		Log.d("14a", "value" + Tab2Activity.studentID_again);

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			// if (take_values.getStringExtra("this_student_id") != null) {
			if (Tab2Activity.studentID_again != null) {
				/*
				 * nameValuePairs.add(new BasicNameValuePair("selectstudent",
				 * take_values.getStringExtra("this_student_id")));
				 */
				nameValuePairs.add(new BasicNameValuePair("selectstudent",
						Tab2Activity.studentID_again));
				Log.d("14a", "in if");

			} else {
				nameValuePairs.add(new BasicNameValuePair("tutorID",
						Tab1Activity.tutorID));
				Log.d("14a", "in else");
			}

			Log.d("14a", "value" + Tab2Activity.studentID_again);

			// Tab2Activity.studentID_again = null;

			Log.d("14a", "value" + Tab2Activity.studentID_again);

			Tab2Activity.studentID_again = null;

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

			Log.d("vc_time", "response : " + responseStr);

			Log.d("go", "check response:  " + responseStr + "tutor id :   "
					+ Tab1Activity.tutorID);

			// Getting JSON Array node
			JSONArray nameList = new JSONArray(responseStr);
			usedForEdit = new JSONArray(responseStr);

			Log.d("13d", "session json" + usedForEdit);
			Log.d("13d", "session json" + usedForEdit.length());

			Log.d("14a", "value" + Tab2Activity.studentID_again
					+ "session json" + usedForEdit.length());

			// looping through All Names

			for (int i = 0; i < nameList.length(); i++) {
				JSONObject n = nameList.getJSONObject(i);

				// Session_to_show_on_list = n.getString("session");
				Student_name_to_show_on_list = n.getString("studentname");
				Student_last_name_to_show_on_list = n.getString("slastname");
				SessionDate_to_show_on_list = n
						.getString("sessionDate");
				Subject_to_show_on_list = n.getString("selectsubject");
				StartTime_to_show_on_list = n.getString("starttime");
				EndTime_to_show_on_list = n.getString("endtime");
				
				SessionDate_to_show_on_list=new SimpleDateFormat("MMM dd, yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(SessionDate_to_show_on_list));
				
				tab3_arraylist.add(new ItemsTab3(R.drawable.ic_launcher,
						"Student: " + Student_name_to_show_on_list + " "
								+ Student_last_name_to_show_on_list,
						"Session Date: " + SessionDate_to_show_on_list,
						"Subject: " + Subject_to_show_on_list,
						StartTime_to_show_on_list + " - "+EndTime_to_show_on_list,"",n.getString("signature")));

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class AsyncTask_ForDeleteRecord extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// Showing progress dialog
			pDialog = new ProgressDialog(Tab3Activity.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData_forDeleteRecord();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			if (pDialog.isShowing()) {
				pDialog.dismiss();
				/**
				 * Updating parsed JSON data into ListView
				 * */

			}

		}

	}

	// this methods Actually send data.
	public void sendData_forDeleteRecord() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "deleteSession.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.

			nameValuePairs.add(new BasicNameValuePair("sessionID", sessionID));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			// now i convert the respone in responseStr

			String responseStr = EntityUtils.toString(response.getEntity());

		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	public void AlertBox() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Tab3Activity.this);

		// set title
		alertDialogBuilder.setTitle("Your Title");

		// set dialog message
		alertDialogBuilder
				.setMessage("Are you really want to delete?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								tab3_arraylist.remove(deletePosition);
								adapter.notifyDataSetChanged();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

}
