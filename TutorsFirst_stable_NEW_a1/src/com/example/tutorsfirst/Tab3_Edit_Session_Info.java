package com.example.tutorsfirst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

public class Tab3_Edit_Session_Info extends Activity implements OnClickListener {

	// Layout Declaration
	TextView tv_selectStudent_number, ed_selectsubject;
	EditText ed_session, ed_outoftotal, ed_sessionnotes;
	TextView ed_starttime, ed_endtime, ed_nextsessiondate,
			ed_nextsessiontime, ed_sessionduration;

	Button btn_save;
	
	ImageView ed_signature;
	TextView btn_audios, btn_images;

	String sessionID, studentID, studentName;

	String prompt_other_value;
	public static String responseStr;

	static final int TIME_DIALOG_ID = 123;
	static final int DATE_PICKER_ID = 321;
	// this variable tells where to set time ed_startTime or ed_endTime;
	static byte setTime_vc;

	// ProgressDialog
	public ProgressDialog pDialog;

	// use only for date picker
	private int year;
	private int month;
	private int day;
	final Calendar c = Calendar.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tab3_edit_session_info);

		init();

		Intent myIntent = getIntent();

		ed_starttime.setOnClickListener(this);
		ed_endtime.setOnClickListener(this);
		ed_nextsessiontime.setOnClickListener(this);
		ed_nextsessiondate.setOnClickListener(this);

		JSONObject Tab3_edit_std_info_object;
		Log.d("stdname", "just up");
		try {
			Tab3_edit_std_info_object = new JSONObject(
					Tab3Activity.Tab3_edit_std_info_object);
			Log.d("testmath_new", Tab3Activity.Tab3_edit_std_info_object);

			studentID = Tab3_edit_std_info_object.getString("studentID");

			Log.d("stdname", Tab3_edit_std_info_object.getString("studentname"));

			studentName = Tab3_edit_std_info_object.getString("studentname");

			/*
			 * tv_selectStudent_number.setText(Tab3_edit_std_info_object
			 * .getString("studentname"));
			 */

			tv_selectStudent_number.setText(Tab3_edit_std_info_object
					.getString("studentname"));

			ed_selectsubject.setText(Tab3_edit_std_info_object
					.getString("selectsubject"));

			ed_session.setText(Tab3_edit_std_info_object.getString("session"));
			ed_outoftotal.setText(Tab3_edit_std_info_object
					.getString("outoftotal"));
			ed_starttime.setText(Tab3_edit_std_info_object
					.getString("starttime"));
			ed_endtime.setText(Tab3_edit_std_info_object.getString("endtime"));
			ed_nextsessiondate.setText(Tab3_edit_std_info_object
					.getString("nextsessiondate"));
			ed_nextsessiontime.setText(Tab3_edit_std_info_object
					.getString("nextsessiontime"));
			ed_sessionduration.setText(Tab3_edit_std_info_object
					.getString("sessionduration"));
			ed_sessionnotes.setText(Tab3_edit_std_info_object
					.getString("sessionnotes"));

			Picasso.with(this)
					.load(MainActivity.BASE_URL + "/UserSignatures/"
							+ Tab3_edit_std_info_object.getString("signature"))
					.into(ed_signature);

			/* .setText(Tab3_edit_std_info_object.getString("")) */

			sessionID = Tab3_edit_std_info_object.getString("sessionID");
			
			ed_signature.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent myIntent = new Intent(Tab3_Edit_Session_Info.this,
							DrawLineUsingGesture.class).putExtra("title", "Edit Signature");
					startActivity(myIntent);
				}
			});
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		// -------------------------------------------------------

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (ed_selectsubject.getText().toString().length() == 0
						|| ed_starttime.getText().toString().length() == 0
						|| ed_endtime.getText().toString().length() == 0
						|| ed_sessionduration.getText().toString().length() == 0
						|| ed_nextsessiondate.getText().toString().length() == 0
						|| ed_nextsessiontime.getText().toString().length() == 0) {
					okButtonAlertbox();
				} else {
					new AsynTask().execute();
					new AsyncUpdateImages().execute();
					new AsyncUpdateAudios().execute();
				}
			}
		});

		// simple code to show dialog box
		ed_selectsubject.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showSelectImageDialog();
			}
		});

		btn_audios.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				new AsyncGetAudios().execute();
			}
		});

		btn_images.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				new SendImages().execute();

			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!(DrawLineUsingGesture.globelTime_for_signature == null)) {
			File file = new File(getCacheDir(),
					DrawLineUsingGesture.globelTime_for_signature);

			Bitmap image_to_be_set_ed_signature = BitmapFactory.decodeFile(file
					.getAbsolutePath());

			ed_signature.setImageBitmap(image_to_be_set_ed_signature);

		}
	}
	
	public void init() {
		tv_selectStudent_number = (TextView) findViewById(R.id.tvSelectStudentText);
		ed_selectsubject = (TextView) findViewById(R.id.tvSelectSubjectText);
		ed_session = (EditText) findViewById(R.id.etSessionNo);
		ed_outoftotal = (EditText) findViewById(R.id.etSessionTotal);
		ed_starttime = (TextView) findViewById(R.id.tvStartTimeText);
		ed_endtime = (TextView) findViewById(R.id.tvEndTimeText);
		ed_nextsessiondate = (TextView) findViewById(R.id.tvNextSesDtText);
		ed_nextsessiontime = (TextView) findViewById(R.id.tvNextSesTimeText);
		ed_sessionduration = (TextView) findViewById(R.id.tvSessionDurationText);
		ed_sessionnotes = (EditText) findViewById(R.id.etSessionNotes);
		ed_signature = (ImageView) findViewById(R.id.ivSignature);
		btn_save = (Button) findViewById(R.id.btnSave);
		btn_audios = (TextView) findViewById(R.id.tvAddAudio);
		btn_images = (TextView) findViewById(R.id.tvAddImage);
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class AsynTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab3_Edit_Session_Info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			// showImageFromURL();
			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}

			// iv_user_pic_tab1.setImageBitmap(image);

		}
	}

	// this methods Actually send data and get Response as JSON.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "editSession.php");

		try {
			// Add your data

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// these is SAME as define in
			// php file.

			nameValuePairs.add(new BasicNameValuePair("selectstudent",
					studentID));

			nameValuePairs.add(new BasicNameValuePair("selectsubject",
					ed_selectsubject.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("session", ed_session
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("outoftotal",
					ed_outoftotal.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("starttime", ed_starttime
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("endtime", ed_endtime
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("sessionduration",
					ed_sessionduration.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("nextsessiondate",
					ed_nextsessiondate.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("nextsessiontime",
					ed_nextsessiontime.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("sessionnotes",
					ed_sessionnotes.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("pictures", "inProcess"));
			nameValuePairs.add(new BasicNameValuePair("audios", "inProcess"));

			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));
			nameValuePairs.add(new BasicNameValuePair("sessionID", sessionID));

			// nameValuePairs.add(new BasicNameValuePair("userpic", imageURL));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			Log.d("NewResponse", "start:  " + response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SendImages extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab3_Edit_Session_Info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			// Create a new HttpClient and Post Header
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(MainActivity.BASE_URL
					+ "Get_ImageID.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			nameValuePairs.add(new BasicNameValuePair("sessionID", sessionID));
			Log.d("18-5", "sessionid:  " + sessionID);

			// Execute HTTP Post Request

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				responseStr = EntityUtils.toString(response.getEntity());
				Log.d("19-1", "server response" + responseStr);

			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}
			Intent jump_Add_Images = new Intent(Tab3_Edit_Session_Info.this,
					TabHostActivity.class);

			TabHostActivity.mangae_tabs = "images_1";

			startActivity(jump_Add_Images);
		}
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// Show selected date
			ed_nextsessiondate.setText(new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year)
					.append(" "));

		}
	};

	private void showSelectImageDialog() {
		Log.d("one", "one");
		AlertDialog.Builder builder = new AlertDialog.Builder(
				Tab3_Edit_Session_Info.this);
		Log.d("one", "two");
		builder.setTitle("Select Image");
		builder.setItems(getResources().getStringArray(R.array.select_subject),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						int pos = (int) Math.round(item);
						ed_selectsubject.setText(getResources().getStringArray(
								R.array.select_subject)[pos]);
						// To open the gallery
						if (pos == 0) {
							ed_selectsubject.setText("SAT");

						} else if (pos == 1) {
							ed_selectsubject.setText("ACT");
						} else if (pos == 2) {
							ed_selectsubject.setText("ISEE");
						} else if (pos == 3) {
							ed_selectsubject.setText("ERB");
						} else if (pos == 4) {
							ed_selectsubject.setText("Academic");
						} else if (pos == 5) {
							PromptAlertbox();
						}

						dialog.dismiss();
					}
				});

		AlertDialog alertBox = builder.create();
		alertBox.show();
	}

	public void PromptAlertbox() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Title");
		alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				prompt_other_value = input.getText().toString();
				ed_selectsubject.setText(input.getText().toString());
				// Do something with value!
			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		alert.show();

	}

	public void okButtonAlertbox() {
		Builder alert = new AlertDialog.Builder(Tab3_Edit_Session_Info.this);

		alert.setTitle("Fill Required Fields");
		alert.setMessage("Please fill all mandatory fiedls");
		alert.setPositiveButton("OK", null);
		alert.show();
	}

	private int hour;
	private int minute;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvStartTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 1;
			break;
		case R.id.tvEndTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 2;
			break;
		case R.id.tvNextSesTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 3;
			break;
		case R.id.tvNextSesDtText:
			showDialog(DATE_PICKER_ID);

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:

			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute,
					false);

		case DATE_PICKER_ID:
			return new DatePickerDialog(this, pickerListener, year, month, day);

		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
			// TODO Auto-generated method stub
			hour = hourOfDay;
			minute = minutes;

			try {
				updateTime(hour, minute);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

	private void updateTime(int hours, int mins) throws ParseException {

		String timeSet = "";
		if (hours > 12) {
			hours -= 12;
			timeSet = "PM";
		} else if (hours == 0) {
			hours += 12;
			timeSet = "AM";
		} else if (hours == 12)
			timeSet = "PM";
		else
			timeSet = "AM";

		String minutes = "";
		if (mins < 10)
			minutes = "0" + mins;
		else
			minutes = String.valueOf(mins);

		// Append in a StringBuilder
		String aTime = new StringBuilder().append(hours).append(':')
				.append(minutes).append(" ").append(timeSet).toString();

		// logic for where to set time ed_starttime or ed_endtime;

		if (setTime_vc == 1) {
			ed_starttime.setText(aTime);

		} else if (setTime_vc == 2) {
			ed_endtime.setText(aTime);

		} else if (setTime_vc == 3) {
			ed_nextsessiontime.setText(aTime);

		}

		if (!(ed_starttime.getText().toString().trim().equalsIgnoreCase("") || ed_endtime
				.getText().toString().trim().equalsIgnoreCase(""))) {
			/*
			 * String a = ed_starttime.getText().toString().replace('P', ' ')
			 * .replace('M', ' ').replace('A', ' ').trim(); String b =
			 * ed_endtime.getText().toString().replace('P', ' ') .replace('M',
			 * ' ').replace('A', ' ').trim();
			 */

			String a = ed_starttime.getText().toString();
			String b = ed_endtime.getText().toString();

			Log.d("2b", a + " : " + b);

			java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm aa");
			java.util.Date date1 = df.parse(a);
			java.util.Date date2 = df.parse(b);

			Log.d("2b", date2.getTime() + " : " + date1.getTime());

			long diff = date2.getTime() - date1.getTime();

			int timeInSeconds = (int) (diff / 1000);
			int hours_for_session_Du, minutes_for_session_du, seconds_for_session_du;
			hours_for_session_Du = timeInSeconds / 3600;
			timeInSeconds = timeInSeconds - (hours_for_session_Du * 3600);
			minutes_for_session_du = timeInSeconds / 60;
			timeInSeconds = timeInSeconds - (minutes_for_session_du * 60);
			// seconds_for_session_du = timeInSeconds;

			Log.d("2b", hours_for_session_Du + ":" + minutes_for_session_du);

			ed_sessionduration.setText(hours_for_session_Du + " Hours  "
					+ minutes_for_session_du + " Minuts");
		}

	}

	class AsyncGetAudios extends AsyncTask<Void, Void, Void> {

		private HttpClient httpClient;
		private HttpPost httpPost;
		private HttpResponse httpResponse;
		private ArrayList<NameValuePair> parameters;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			responseStr=null;
			pDialog = ProgressDialog.show(Tab3_Edit_Session_Info.this, "",
					"Please wait...");
			httpClient = new DefaultHttpClient();
			httpPost = new HttpPost(MainActivity.BASE_URL + "/GetAudio.php");
			parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair("sessionID", sessionID));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(parameters));
				httpResponse = httpClient.execute(httpPost);
				responseStr = EntityUtils.toString(httpResponse.getEntity());
				Log.i("audio_response", responseStr);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pDialog.cancel();
			Intent myIntent = new Intent(Tab3_Edit_Session_Info.this,
					TabHostActivity.class);
			TabHostActivity.mangae_tabs = "audios_1";
			startActivity(myIntent);
		}
	}
	
	class AsyncUpdateImages extends AsyncTask<Void, Void, Void>{

		HttpClient httpClient;
		HttpPost httpPost;
		MultipartEntityBuilder builder;
		HttpResponse httpResponse;
		String response;
		ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			progressDialog=ProgressDialog.show(Tab3_Edit_Session_Info.this, "", "Please wait...");
			httpClient=new DefaultHttpClient();
			httpPost=new HttpPost(MainActivity.BASE_URL+"/upload_SessionImages.php");
			builder=MultipartEntityBuilder.create();
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			if(ImagesInGridActivity.image_upload_index>=0){
				for(int i=ImagesInGridActivity.image_upload_index;i<ImagesInGridActivity.gridArray.size();i++){
					File cacheFile=new File(getCacheDir(),new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".png");
					Bitmap bitmap=ImagesInGridActivity.gridArray.get(i).image;
					try {
						bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(cacheFile));
						builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
						builder.addPart("file", new FileBody(cacheFile));
						builder.addTextBody("sessionID", sessionID);
						httpPost.setEntity(builder.build());
						httpResponse=httpClient.execute(httpPost);
						response=EntityUtils.toString(httpResponse.getEntity());
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					cacheFile.delete();
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.cancel();
		}
	}
	
	class AsyncUpdateAudios extends AsyncTask<Void, Void, Void>{

		HttpClient httpClient;
		HttpPost httpPost;
		MultipartEntityBuilder builder;
		HttpResponse httpResponse;
		String response;
		ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=ProgressDialog.show(Tab3_Edit_Session_Info.this, "", "Please wait...");
			httpClient=new DefaultHttpClient();
			httpPost=new HttpPost(MainActivity.BASE_URL+"/upload_SessionAudios.php");
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			if(AddAudios.audio_upload_index>=0 && AddAudios.Audios_arraylist!=null){
				for(int i=AddAudios.audio_upload_index;i<AddAudios.Audios_arraylist.size();i++){
					File cacheFile=new File(AddAudios.Audios_arraylist.get(i).filepath);
					builder=MultipartEntityBuilder.create();
					builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					builder.addPart("file", new FileBody(cacheFile));
					builder.addTextBody("sessionID", sessionID);
					httpPost.setEntity(builder.build());
					try {
						httpResponse=httpClient.execute(httpPost);
						response=EntityUtils.toString(httpResponse.getEntity());
						Log.i("Uploaded "+i, response);
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.cancel();
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		responseStr=null;
		if(AddAudios.Audios_arraylist!=null){
			AddAudios.Audios_arraylist.clear();
		}
		if(ImagesInGridActivity.gridArray!=null){
			ImagesInGridActivity.gridArray.clear();
		}
	}
}
