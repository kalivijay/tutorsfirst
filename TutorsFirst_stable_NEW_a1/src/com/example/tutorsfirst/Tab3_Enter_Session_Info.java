package com.example.tutorsfirst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class Tab3_Enter_Session_Info extends Activity implements
		OnClickListener {

	// Layout Declaration
	TextView tv_selectStudent_number,ed_selectsubject,ed_starttime,ed_endtime,ed_sessionduration,ed_nextsessiondate,ed_nextsessiontime;
	EditText ed_session, ed_outoftotal, ed_sessionnotes;
	ImageView ed_signature;
	TextView btn_audios, btn_images;
	Button btn_save;
	public ProgressDialog pDialog;
	// Textview
	TextView tv_navi_login;

	Bitmap image;
	// this is to be set on the student signature.
	Bitmap image_to_be_set_ed_signature;

	String prompt_other_value;
	// this is sessionId , i get it when i call addSession.php
	String SessionID;

	// it is asign by a value when user clicks on "select student"
	static String studentID_of_selected_studnet = "a";
	// this variable put 1 student selected.
	static String select_stuent_label = "0";

	// use only for date picker
	private int year;
	private int month;
	private int day;
	final Calendar c = Calendar.getInstance();
	ArrayList<AddAudiosModel> list;
	ArrayList<Item> list_image;

	String responseStr;

	public static Model_Tab3_Enter_Session_Vaules Model_Tab3_Enter_Session_Vaules_ob;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tab3_enter_session_info);

		init();

		if (AddAudios.Audios_arraylist == null) {
			Log.d("18-3", "null");
		} else {
			Log.d("18-3", "audioslist:   " + AddAudios.Audios_arraylist
					+ "     audiosSize:   " + AddAudios.Audios_arraylist.size());
			for (int i = 0; i < AddAudios.Audios_arraylist.size(); i++) {
				String a = AddAudios.Audios_arraylist.get(i).filepath;
				Log.d("18-3", a);
			}
		}

		if (ImagesInGridActivity.gridArray == null) {
			Log.d("18-4", "null");
		} else {
			for (int i = 0; i < ImagesInGridActivity.gridArray.size(); i++) {
				Bitmap a = ImagesInGridActivity.gridArray.get(i).image;
				// Log.d("18-3", a);
			}
		}

		Intent pass_audios = getIntent();
		list = (ArrayList<AddAudiosModel>) pass_audios
				.getSerializableExtra("pass_audios");

		list_image = ImagesInGridActivity.gridArray;

		Log.d("18d", "inTab3Enter:  " + list_image);

		/*
		 * Model_Tab3_Enter_Session_Vaules model_Tab3_Enter_Session_Vaules_obj =
		 * (Model_Tab3_Enter_Session_Vaules) pass_audios
		 * .getSerializableExtra("pass_values_ob");
		 */

		if (Model_Tab3_Enter_Session_Vaules_ob == null) {
			Log.d("15d", "null");
		} else {
			Log.d("15d", "start Time:   "
					+ Model_Tab3_Enter_Session_Vaules_ob.ed_starttime);

			ed_selectsubject
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_selectsubject);

			ed_session.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_session);

			ed_outoftotal
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_outoftotal);

			ed_starttime
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_starttime);

			ed_endtime.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_endtime);

			ed_nextsessiondate
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_nextsessiondate);

			ed_nextsessiontime
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_nextsessiontime);

			ed_sessionnotes
					.setText(Model_Tab3_Enter_Session_Vaules_ob.ed_sessionnotes);

		}

		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		ed_signature.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent myIntent = new Intent(Tab3_Enter_Session_Info.this,
						DrawLineUsingGesture.class).putExtra("title", "Add Signature");
				startActivity(myIntent);
			}
		});
		// simple code to show dialog box
		ed_selectsubject.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showSelectImageDialog();
			}
		});

		ed_starttime.setOnClickListener(this);
		ed_endtime.setOnClickListener(this);
		ed_nextsessiontime.setOnClickListener(this);
		ed_nextsessiondate.setOnClickListener(this);

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (ed_selectsubject.getText().toString().length() == 0
						|| ed_starttime.getText().toString().length() == 0
						|| ed_endtime.getText().toString().length() == 0
						|| ed_sessionduration.getText().toString().length() == 0
						|| ed_nextsessiondate.getText().toString().length() == 0
						|| ed_nextsessiontime.getText().toString().length() == 0) {
					okButtonAlertbox();
				} else {
					new SaveData().execute();
					new SendUserSign().execute();
					
					Toast.makeText(getApplicationContext(), "Data Saved",
							Toast.LENGTH_SHORT).show();

					Intent myIntent = new Intent(Tab3_Enter_Session_Info.this,
							TabHostActivity.class);
					TabHostActivity.mangae_tabs = "tab3shown";
					startActivity(myIntent);

				}

			}
		});

		tv_selectStudent_number.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*
				 * Intent jump_StudentDataWithCheckboxes = new Intent(
				 * Tab3_Enter_Session_Info.this,
				 * StudentData_with_Checkboxs.class);
				 */
				Intent jump_StudentDataWithCheckboxes = new Intent(
						Tab3_Enter_Session_Info.this, TabHostActivity.class);
				TabHostActivity.mangae_tabs = "7";
				startActivity(jump_StudentDataWithCheckboxes);
			}
		});

		btn_audios.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (list != null) {
					Log.d("values", list.toString());
				}

				/*
				 * startActivity(new Intent(Tab3_Enter_Session_Info.this,
				 * AddAudios.class).putExtra("pass_audios", list));
				 */

				// getting values all values and send to model.

				Model_Tab3_Enter_Session_Vaules_ob = new Model_Tab3_Enter_Session_Vaules(
						ed_selectsubject.getText().toString(), ed_session
								.getText().toString(), ed_outoftotal.getText()
								.toString(), ed_starttime.getText().toString(),
						ed_endtime.getText().toString(), ed_nextsessiondate
								.getText().toString(), ed_nextsessiontime
								.getText().toString(), ed_sessionnotes
								.getText().toString());

				// ----------------------------------------------

				TabHostActivity.mangae_tabs = "audios_1";

				Intent jump_Add_Audios = new Intent(
						Tab3_Enter_Session_Info.this, TabHostActivity.class);

				jump_Add_Audios.putExtra("pass_audios", list);
				jump_Add_Audios.putExtra("pass_values_ob",
						Model_Tab3_Enter_Session_Vaules_ob);

				/*
				 * startActivity(new Intent(Tab3_Enter_Session_Info.this,
				 * TabHostActivity.class).putExtra("pass_audios", list));
				 */

				startActivity(jump_Add_Audios);

				finish();
			}
		});

		btn_images.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*
				 * Intent myIntent = new Intent(Tab3_Enter_Session_Info.this,
				 * ImagesInGridActivity.class);
				 */

				Intent jump_Add_Images = new Intent(
						Tab3_Enter_Session_Info.this, TabHostActivity.class);

				TabHostActivity.mangae_tabs = "images_1";

				// jump_Add_Images.putExtra("pass_images", list_image);

				startActivity(jump_Add_Images);
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (!select_stuent_label.equalsIgnoreCase("0")) {
			tv_selectStudent_number.setText(select_stuent_label + "");
		}
		Log.d("check", DrawLineUsingGesture.globelTime_for_signature
				+ ": value");

		if (!(DrawLineUsingGesture.globelTime_for_signature == null)) {
			File file = new File(getCacheDir(),
					DrawLineUsingGesture.globelTime_for_signature);

			image_to_be_set_ed_signature = BitmapFactory.decodeFile(file
					.getAbsolutePath());

			ed_signature.setImageBitmap(image_to_be_set_ed_signature);

			// ed_signature.

		}

		/*
		 * File file = new File(getCacheDir(),
		 * DrawLineUsingGesture.globelTime_for_signature);
		 */
	}

	// To fetch the image from gallery
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

			Cursor cursor = getContentResolver().query(data.getData(),
					new String[] { MediaStore.Images.Media.DATA }, null, null,
					null);
			if (cursor.moveToNext()) {
				String filePath = cursor.getString(cursor
						.getColumnIndex(MediaStore.Images.Media.DATA));
				image = BitmapFactory.decodeFile(filePath);

				// btn_images.setImageBitmap(image);

			}
			cursor.close();

		}

	}

	public void init() {

		tv_selectStudent_number = (TextView) findViewById(R.id.tvSelectStudentText);
		ed_selectsubject = (TextView) findViewById(R.id.tvSelectSubjectText);
		ed_session = (EditText) findViewById(R.id.etSessionNo);
		ed_outoftotal = (EditText) findViewById(R.id.etSessionTotal);
		ed_starttime = (TextView) findViewById(R.id.tvStartTimeText);
		ed_endtime = (TextView) findViewById(R.id.tvEndTimeText);
		ed_nextsessiondate = (TextView) findViewById(R.id.tvNextSesDtText);
		ed_nextsessiontime = (TextView) findViewById(R.id.tvNextSesTimeText);
		ed_sessionduration = (TextView) findViewById(R.id.tvSessionDurationText);
		ed_sessionnotes = (EditText) findViewById(R.id.etSessionNotes);
		ed_signature = (ImageView) findViewById(R.id.ivSignature);
		btn_save = (Button) findViewById(R.id.btnSave);
		btn_audios = (TextView) findViewById(R.id.tvAddAudio);
		btn_images = (TextView) findViewById(R.id.tvAddImage);
		SpannableString content = new SpannableString("Add Audio");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		btn_audios.setText(content);
		content = new SpannableString("Add Image");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		btn_images.setText(content);
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SaveData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Tab3_Enter_Session_Info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			sendData();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Dismiss the Progress Bar

			// Log.d("Time", arraylist + "");

			if (pDialog.isShowing()) {
				pDialog.dismiss();

			}
			
			new SendAudios().execute();
			new SendImages().execute();
		}
	}

	// this methods Actually send data.
	public void sendData() {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "addSession.php");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			// these name,email,phone,password and userpic is SAME as define in
			// php file.
			/*
			 * nameValuePairs.add(new BasicNameValuePair("selectstudent",
			 * tv_selectStudent_number.getText().toString()));
			 */
			Log.d("peh", "in tab3 asytask" + studentID_of_selected_studnet);
			nameValuePairs.add(new BasicNameValuePair("selectstudent",
					studentID_of_selected_studnet));

			studentID_of_selected_studnet = "a";

			Log.d("121a", tv_selectStudent_number.getText().toString());
			nameValuePairs.add(new BasicNameValuePair("selectsubject",
					ed_selectsubject.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("session", ed_session
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("outoftotal",
					ed_outoftotal.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("starttime", ed_starttime
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("endtime", ed_endtime
					.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("sessionduration",
					ed_sessionduration.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("nextsessiondate",
					ed_nextsessiondate.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("nextsessiontime",
					ed_nextsessiontime.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("sessionnotes",
					ed_sessionnotes.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("pictures", "inProcess"));
			// MyAdapterAudios.s

			nameValuePairs.add(new BasicNameValuePair("sessionDate",
					"inProcess"));

			nameValuePairs.add(new BasicNameValuePair("signature",
					DrawLineUsingGesture.globelTime_for_signature));

			// i change this value
			// DrawLineUsingGesture.globelTime_for_signature = "No sign";

			nameValuePairs.add(new BasicNameValuePair("tutorID",
					Tab1Activity.tutorID));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);

			responseStr = EntityUtils.toString(response.getEntity());

			Log.d("18-2", "responseStr:   " + responseStr);

			JSONArray arr = new JSONArray(responseStr);

			if (arr.length() != 0) {
				Log.d("javavk", arr + "");

				JSONObject jObj = arr.getJSONObject(0);

				SessionID = jObj.getString("sessionID");

			}
			Log.d("18-5", "SessionId:   " + SessionID);

		}/*
		 * catch (ClientProtocolException e) { // TODO Auto-generated catch
		 * block }
		 */catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SendUserSign extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {

			uploadImage();

			return null;
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SendAudios extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			if (AddAudios.Audios_arraylist != null) {
				uploadAudios();
			}

			return null;
		}

	}

	/**
	 * Async task class to get json by making HTTP call
	 * */
	private class SendImages extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			uploadImages_from_imagesInGrid();
			return null;
		}

	}

	private int hour;
	private int minute;

	public int startTime_or_endTime;

	static final int TIME_DIALOG_ID = 123;
	static final int DATE_PICKER_ID = 321;
	// this variable tells where to set time ed_startTime or ed_endTime;
	static byte setTime_vc;

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvStartTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 1;
			break;
		case R.id.tvEndTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 2;
			break;
		case R.id.tvNextSesTimeText:
			showDialog(TIME_DIALOG_ID);
			setTime_vc = 3;
			break;
		case R.id.tvNextSesDtText:
			showDialog(DATE_PICKER_ID);

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:

			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute,
					false);

		case DATE_PICKER_ID:
			return new DatePickerDialog(this, pickerListener, year, month, day);

		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
			// TODO Auto-generated method stub
			hour = hourOfDay;
			minute = minutes;

			try {
				updateTime(hour, minute);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

	private void updateTime(int hours, int mins) throws ParseException {

		String timeSet = "";
		if (hours > 12) {
			hours -= 12;
			timeSet = "PM";
		} else if (hours == 0) {
			hours += 12;
			timeSet = "AM";
		} else if (hours == 12)
			timeSet = "PM";
		else
			timeSet = "AM";

		String minutes = "";
		if (mins < 10)
			minutes = "0" + mins;
		else
			minutes = String.valueOf(mins);

		// Append in a StringBuilder
		String aTime = new StringBuilder().append(hours).append(':')
				.append(minutes).append(" ").append(timeSet).toString();

		// logic for where to set time ed_starttime or ed_endtime;

		if (setTime_vc == 1) {
			ed_starttime.setText(aTime);

		} else if (setTime_vc == 2) {
			ed_endtime.setText(aTime);

		} else if (setTime_vc == 3) {
			ed_nextsessiontime.setText(aTime);

		}

		if (!(ed_starttime.getText().toString().trim().equalsIgnoreCase("") || ed_endtime
				.getText().toString().trim().equalsIgnoreCase(""))) {
			/*
			 * String a = ed_starttime.getText().toString().replace('P', ' ')
			 * .replace('M', ' ').replace('A', ' ').trim(); String b =
			 * ed_endtime.getText().toString().replace('P', ' ') .replace('M',
			 * ' ').replace('A', ' ').trim();
			 */

			String a = ed_starttime.getText().toString();
			String b = ed_endtime.getText().toString();

			Log.d("2b", a + " : " + b);

			java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm aa");
			java.util.Date date1 = df.parse(a);
			java.util.Date date2 = df.parse(b);

			Log.d("2b", date2.getTime() + " : " + date1.getTime());

			long diff = date2.getTime() - date1.getTime();

			int timeInSeconds = (int) (diff / 1000);
			int hours_for_session_Du, minutes_for_session_du, seconds_for_session_du;
			hours_for_session_Du = timeInSeconds / 3600;
			timeInSeconds = timeInSeconds - (hours_for_session_Du * 3600);
			minutes_for_session_du = timeInSeconds / 60;
			timeInSeconds = timeInSeconds - (minutes_for_session_du * 60);
			// seconds_for_session_du = timeInSeconds;

			Log.d("2b", hours_for_session_Du + ":" + minutes_for_session_du);

			ed_sessionduration.setText(hours_for_session_Du + " Hours  "
					+ minutes_for_session_du + " Minuts");
		}

	}

	private void showSelectImageDialog() {
		Log.d("one", "one");
		AlertDialog.Builder builder = new AlertDialog.Builder(
				Tab3_Enter_Session_Info.this);
		Log.d("one", "two");
		builder.setTitle("Select Image");
		builder.setItems(getResources().getStringArray(R.array.select_subject),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						int pos = (int) Math.round(item);
						ed_selectsubject.setText(getResources().getStringArray(
								R.array.select_subject)[pos]);
						// To open the gallery
						if (pos == 0) {
							ed_selectsubject.setText("SAT");

						} else if (pos == 1) {
							ed_selectsubject.setText("ACT");
						} else if (pos == 2) {
							ed_selectsubject.setText("ISEE");
						} else if (pos == 3) {
							ed_selectsubject.setText("ERB");
						} else if (pos == 4) {
							ed_selectsubject.setText("Academic");
						} else if (pos == 5) {
							PromptAlertbox();
						}

						dialog.dismiss();
					}
				});

		AlertDialog alertBox = builder.create();
		alertBox.show();
	}

	public void PromptAlertbox() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Title");
		alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				prompt_other_value = input.getText().toString();
				ed_selectsubject.setText(input.getText().toString());
				// Do something with value!
			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		alert.show();

	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// Show selected date
			ed_nextsessiondate.setText(new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year)
					.append(" "));

		}
	};

	public void okButtonAlertbox() {
		Builder alert = new AlertDialog.Builder(Tab3_Enter_Session_Info.this);

		alert.setTitle("Fill Required Fields");
		alert.setMessage("Please fill all mandatory fiedls");
		alert.setPositiveButton("OK", null);
		alert.show();
	}

	public void uploadImage() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();

		// File file = new File(getCacheDir(), imageURL);
		File file_ob = new File(getCacheDir(),
				DrawLineUsingGesture.globelTime_for_signature);
		try {
			image_to_be_set_ed_signature.compress(Bitmap.CompressFormat.PNG,
					90, new FileOutputStream(file_ob));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		mpBuilder.addPart("file", new FileBody(file_ob));
		HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
				+ "uploadUserSign.php");
		HttpClient httpClient = new DefaultHttpClient();
		httpPost_image.setEntity(mpBuilder.build());

		try {
			HttpResponse httpResponse = httpClient.execute(httpPost_image);

			String response = EntityUtils.toString(httpResponse.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// return null;
	}

	public void uploadAudios() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();

		for (int i = 0; i < AddAudios.Audios_arraylist.size(); i++) {
			String a = AddAudios.Audios_arraylist.get(i).filepath;
			Log.d("18-3", a);
			File file_ob = new File(a);
			mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			mpBuilder.addPart("file", new FileBody(file_ob));
			mpBuilder.addTextBody("sessionID", SessionID);
			HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
					+ "upload_SessionAudios.php");
			HttpClient httpClient = new DefaultHttpClient();
			httpPost_image.setEntity(mpBuilder.build());

			try {
				HttpResponse httpResponse = httpClient.execute(httpPost_image);

				String response = EntityUtils
						.toString(httpResponse.getEntity());
				Log.d("18-3", "from server:   " + response);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		// return null;
	}

	public void uploadImages_from_imagesInGrid() {
		MultipartEntityBuilder mpBuilder = MultipartEntityBuilder.create();

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(MainActivity.BASE_URL
				+ "upload_SessionImages.php");

		// Add your data
		//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

		//nameValuePairs.add(new BasicNameValuePair("sessionID", SessionID));
		//nameValuePairs.add(new BasicNameValuePair("audioname", SessionID));
		Log.d("18-5", "sessionid_whenSends   :  " + SessionID);// audioname

		for (int i = 0; i < ImagesInGridActivity.gridArray.size(); i++) {

			Bitmap a = ImagesInGridActivity.gridArray.get(i).image;

			String file_name = DrawLineUsingGesture.globelTime_for_signature
					.replace(".png", "");
			
			//nameValuePairs.add(new BasicNameValuePair("audioname", file_name));

			File file_ob = new File(getCacheDir(), file_name + "_" + i + ".png");

			Log.d("18-4", "timeStamp:   " + file_name + "_" + i + ".png");
			
			try {
				a.compress(Bitmap.CompressFormat.PNG, 90, new FileOutputStream(
						file_ob));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mpBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			mpBuilder.addPart("file", new FileBody(file_ob));
			mpBuilder.addTextBody("sessionID", SessionID);
			HttpPost httpPost_image = new HttpPost(MainActivity.BASE_URL
					+ "upload_SessionImages.php");
			HttpClient httpClient = new DefaultHttpClient();
			httpPost_image.setEntity(mpBuilder.build());

			try {
				HttpResponse httpResponse = httpClient.execute(httpPost_image);

				String response = EntityUtils
						.toString(httpResponse.getEntity());
				Log.d("18-4", "from server:   " + response);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		// Execute HTTP Post Request

		/*try {
			HttpResponse response = httpclient.execute(httppost);
			responseStr = EntityUtils.toString(response.getEntity());
			Log.d("18-5", "server response" + responseStr);
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/

		/*try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		// return null;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		DrawLineUsingGesture.globelTime_for_signature = null;
		super.onBackPressed();
		finish();
	}
}
