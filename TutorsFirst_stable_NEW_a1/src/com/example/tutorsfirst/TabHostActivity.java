package com.example.tutorsfirst;

import java.util.ArrayList;

import android.app.ActionBar.Tab;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

public class TabHostActivity extends TabActivity implements OnTabChangeListener{

	// Define:
	// tab0 home
	// tab1 my profile
	// tab2 students
	// tab3 sessions

	// Layout Declaration
	Button btn_sign_up;
	public static String mangae_tabs = "0";
	TabHost tabHost;
	ArrayList<AddAudiosModel> list;
	String arrTabSpec[] = new String[] { "Home info", "Profile Info",
			"Student Info", "Session Info", "Add Student", "Add Session",
			"Add Session", "Edit Session", "Edit Session", "Edit Session",
			"Std With Checkbox", "Add Session", "Add Session" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab_host);

		// create the TabHost that will contain the Tabs
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		TabWidget tabWidget = tabHost.getTabWidget();
		tabHost.setOnTabChangedListener(this);
		tabWidget.setStripEnabled(false);

		TabSpec tab0 = tabHost.newTabSpec("Home info");
		TabSpec tab1 = tabHost.newTabSpec("Profile Info");
		TabSpec tab2 = tabHost.newTabSpec("Student Info");
		TabSpec tab3 = tabHost.newTabSpec("Session Info");
		// ------Add new std
		TabSpec tab4_for_add_std = tabHost.newTabSpec("Add Student");
		tab4_for_add_std.setIndicator(createView("Students"));
		tab4_for_add_std
				.setContent(new Intent(this, Tab2_Enter_stud_info.class));
		// ----------Add new Session
		TabSpec tab5_for_add_sess = tabHost.newTabSpec("Add Session");
		tab5_for_add_sess.setIndicator(createView("Sessions"));

		Intent jump_tab3_enter_session_info = new Intent(this,
				Tab3_Enter_Session_Info.class);
		Intent getValues = getIntent();

		jump_tab3_enter_session_info.putExtra("pass_audios",
				getValues.getSerializableExtra("pass_audios"));

		jump_tab3_enter_session_info.putExtra("pass_values_ob",
				getValues.getSerializableExtra("pass_values_ob"));

		tab5_for_add_sess.setContent(jump_tab3_enter_session_info);

		// ----------Add new Session from imagessinGridActivity

		TabSpec tab5_for_add_sess_images = tabHost.newTabSpec("Add Session");
		tab5_for_add_sess_images.setIndicator(createView("Sessions"));

		Intent jump_tab3_enter_session_info_images = new Intent(this,
				Tab3_Enter_Session_Info.class);
		// Intent getValues = getIntent();

		/*
		 * jump_tab3_enter_session_info_images.putExtra("pass_images",
		 * getValues.getSerializableExtra("pass_images"));
		 */

		tab5_for_add_sess_images
				.setContent(jump_tab3_enter_session_info_images);

		// ---------- Session List
		TabSpec tab6_for_add_sess = tabHost.newTabSpec("Edit Session");
		tab6_for_add_sess.setIndicator(createView("Sessions"));
		tab6_for_add_sess.setContent(new Intent(this, Tab3Activity.class));

		// ----------Edit Std
		TabSpec tab7_for_edit_std = tabHost.newTabSpec("Edit Student");

		tab7_for_edit_std.setIndicator(createView("Students"));

		tab7_for_edit_std
				.setContent(new Intent(this, Tab2_Edit_Std_Info.class));

		// ---------- Session List
		TabSpec tab8_for_edit_sess = tabHost.newTabSpec("Edit Session");
		tab8_for_edit_sess.setIndicator(createView("Sessions"));
		tab8_for_edit_sess.setContent(new Intent(this,
				Tab3_Edit_Session_Info.class));

		// ---------- Std With checkboxs----------------
		TabSpec tab9_for_edit_sess = tabHost.newTabSpec("Std With Checkbox");
		tab9_for_edit_sess.setIndicator(createView("Sessions"));
		tab9_for_edit_sess.setContent(new Intent(this,
				StudentData_with_Checkboxs.class));

		// ---------------------tab 0-------------------------------------
		tab0.setIndicator(createView("Home"));
		tab0.setContent(new Intent(this, HomeActivity.class));

		// -----------------AddAudio-----------------------------
		TabSpec tab10_for_add_sess = tabHost.newTabSpec("Add Session");
		tab10_for_add_sess.setIndicator(createView("Sessions"));

		Intent jump_tab10_enter_session_info = new Intent(this, AddAudios.class);

		jump_tab10_enter_session_info.putExtra("pass_audios",
				getValues.getSerializableExtra("pass_audios"));

		jump_tab10_enter_session_info.putExtra("pass_values_ob",
				getValues.getSerializableExtra("pass_values_ob"));

		tab10_for_add_sess.setContent(jump_tab10_enter_session_info);

		// -----------------AddImages-----------------------------
		TabSpec tab10_for_add_images = tabHost.newTabSpec("Add Session");
		tab10_for_add_images.setIndicator(createView("Sessions"));

		Intent jump_tab10_enter_session_info_images = new Intent(this,
				ImagesInGridActivity.class);

		/*
		 * jump_tab10_enter_session_info_images.putExtra("pass_images",
		 * getValues.getSerializableExtra("pass_images"));
		 */

		tab10_for_add_images.setContent(jump_tab10_enter_session_info_images);

		// -------------------------------------------------------------

		// Intent getValues = getIntent();

		Log.d("28a", "" + getValues.getStringExtra("email"));

		// Set the Tab name and Activity
		// that will be opened when particular Tab will be selected
		tab1.setIndicator(createView("My Profile"));

		Intent jump_tab1activity = new Intent(this, Tab1Activity.class);

		// get values from LoginPage and send to tablactivity
		jump_tab1activity.putExtra("email", getValues.getStringExtra("email"));
		jump_tab1activity.putExtra("name", getValues.getStringExtra("name"));
		jump_tab1activity.putExtra("phone", getValues.getStringExtra("phone"));
		jump_tab1activity.putExtra("lastname",
				getValues.getStringExtra("last_name"));
		jump_tab1activity.putExtra("password",
				getValues.getStringExtra("password"));
		jump_tab1activity.putExtra("userpic",
				getValues.getStringExtra("userpic"));
		jump_tab1activity.putExtra("tutorID",
				getValues.getStringExtra("tutorID"));

		tab1.setContent(jump_tab1activity);

		// -------------------------------------------------------------------

		// get values from tab2activity and send to tab2_edit_std

		/*
		 * jump_tab1activity.putExtra("json_from_tab2activity",
		 * getValues.getStringExtra("json_from_tab2activity"));
		 */

		// -------------------------------------------

		tab2.setIndicator(createView("Students"));
		tab2.setContent(new Intent(this, Tab2Activity.class));

		tab3.setIndicator(createView("Sessions"));
		tab3.setContent(new Intent(this, PreTab3Activity.class));

		/*
		 * // tabs color for (int i = 0; i <
		 * tabHost.getTabWidget().getChildCount(); i++) { TextView tv =
		 * (TextView) tabHost.getTabWidget().getChildAt(i)
		 * .findViewById(android.R.id.title);
		 * tv.setTextColor(Color.parseColor("#ffffff")); }
		 */

		/** Add the tabs to the TabHost to display. */
		Log.d("7a", " : " + mangae_tabs);
		// 0 for defaults condition
		// 1 when add_new_std activity should be shown
		// 2 when back pressed after add_new_std or other.
		// 3 when add_new_sess activity should be shown.
		// 4 when sess_list activity should be shown.
		// 5 when edit_std activity should be shown.
		// 6 when edit_session should be shown
		// 7 when std_with_checkboxes should be shown
		// audios when i come from AddAudios.java activity and go to
		// Tab3_Enter_session_info.

		// audios_1 when i come from tab3_enter_session_info and go to add
		// AddAudios.java

		if (mangae_tabs.equalsIgnoreCase("0")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab3);
			tabHost.addTab(tab1);
		} else if (mangae_tabs.equalsIgnoreCase("1")) {

			tabHost.addTab(tab0);
			tabHost.addTab(tab4_for_add_std);
			tabHost.addTab(tab3);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(1);

		} else if (mangae_tabs.equalsIgnoreCase("5")) {

			tabHost.addTab(tab0);
			tabHost.addTab(tab7_for_edit_std);
			tabHost.addTab(tab3);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(1);

		} else if (mangae_tabs.equalsIgnoreCase("tab2shown")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab3);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(1);
		}

		else if (mangae_tabs.equalsIgnoreCase("6")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab8_for_edit_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		}
		if (mangae_tabs.equalsIgnoreCase("tab3shown")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab3);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		} else if (mangae_tabs.equalsIgnoreCase("3")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab5_for_add_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		}

		else if (mangae_tabs.equalsIgnoreCase("4")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab6_for_add_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		} else if (mangae_tabs.equalsIgnoreCase("7")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab9_for_edit_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		} else if (mangae_tabs.equalsIgnoreCase("audios")) {

			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab5_for_add_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		}

		else if (mangae_tabs.equalsIgnoreCase("audios_1")) {

			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab10_for_add_sess);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);
		}

		else if (mangae_tabs.equalsIgnoreCase("images_1")) {
			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab10_for_add_images);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);

		}

		else if (mangae_tabs.equalsIgnoreCase("images")) {

			tabHost.addTab(tab0);
			tabHost.addTab(tab2);
			tabHost.addTab(tab5_for_add_sess_images);
			tabHost.addTab(tab1);
			tabHost.setCurrentTab(2);

		}

		/*
		 * else if (mangae_tabs.equalsIgnoreCase("1")) {
		 * 
		 * tabHost.addTab(tab0); tabHost.addTab(tab2);
		 * tabHost.addTab(tab4_for_add_std); tabHost.addTab(tab1);
		 * 
		 * tabHost.setCurrentTab(2);
		 * 
		 * } else if (mangae_tabs.equalsIgnoreCase("2")) { tabHost.addTab(tab0);
		 * tabHost.addTab(tab2); tabHost.addTab(tab3); tabHost.addTab(tab1);
		 * tabHost.setCurrentTab(2); }
		 *//*
			 * else if (mangae_tabs.equalsIgnoreCase("3")) {
			 * tabHost.addTab(tab0); tabHost.addTab(tab1); tabHost.addTab(tab2);
			 * 
			 * tabHost.addTab(tab5_for_add_sess); tabHost.setCurrentTab(3); }
			 * else if (mangae_tabs.equalsIgnoreCase("4")) {
			 * tabHost.addTab(tab0); tabHost.addTab(tab1); tabHost.addTab(tab2);
			 * 
			 * tabHost.addTab(tab6_for_add_sess); tabHost.setCurrentTab(3); }
			 */

		/*
		 * else if (mangae_tabs.equalsIgnoreCase("5")) { tabHost.addTab(tab0);
		 * tabHost.addTab(tab1); tabHost.addTab(tab7_for_edit_std);
		 * 
		 * tabHost.addTab(tab3); tabHost.setCurrentTab(2); } else if
		 * (mangae_tabs.equalsIgnoreCase("6")) { tabHost.addTab(tab0);
		 * tabHost.addTab(tab1); tabHost.addTab(tab2);
		 * 
		 * tabHost.addTab(tab8_for_edit_sess); tabHost.setCurrentTab(3); }
		 */

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	private View createView(String title) {
		LinearLayout view = (LinearLayout) LayoutInflater.from(
				TabHostActivity.this).inflate(R.layout.tab_indicator, null);
		TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		tvTitle.setText(title);
		return view;
	}

	@Override
	public void onTabChanged(String tabID) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(tabHost.getApplicationWindowToken(), 0);

		for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
			if (tabID.equalsIgnoreCase(arrTabSpec[i])) {
				tabHost.getTabWidget().getChildAt(i)
						.setBackgroundColor(Color.parseColor("#453b2f"));
			} else {
				tabHost.getTabWidget().getChildAt(i)
						.setBackgroundColor((Color.parseColor("#635543")));
			}
		}
	}
}
